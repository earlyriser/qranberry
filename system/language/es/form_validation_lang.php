<?php

$lang['required'] 		= "El campo %s es requerido.";
$lang['isset']			= "El campo %s debe contener un valor.";
$lang['valid_email']	= "El campo %s debe contener una dirección de correo válida.";
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['valid_url'] 		= "El campo %s debe contener una URL válida.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']		= "El campo %s debe contener al menos %s carácteres de largo.";
$lang['max_length']		= "El campo %s no debe exceder los %s carácteres de largo.";
$lang['exact_length']	= "El campo %s debe tener exactamente %s carácteres de largo.";
$lang['alpha']			= "El campo %s solamente puede contener carácteres alfanuméricos.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']		= "El campo %s solamente puede contener carácteres alfanuméricos, guion y guion bajo.";
$lang['numeric']		= "El campo %s debe contener números.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']		= "El campo %s no es igual al campo %s.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";







/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
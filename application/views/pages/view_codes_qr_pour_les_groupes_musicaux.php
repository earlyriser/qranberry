<div class="page">
	<h1>Codes QR pour groupes musicaux</h1>
	
	<div class="page_content">

		<section>
			<h2>Avantages</h2>
			<ul>
				<li>Faites connaître votre musique en présentant des extraits sur des feuilles volantes ou pochettes de CD.</li>
				<li>Partez à l'assaut des médias sociaux avec une page mobile qui rend le partage facile (un clic de bouton)é</li>
			</ul>
		</section>
		
		<section>
			<h2>Démo</h2>
			<p>
				<a href="">Démo groupe</a><br>
				Scannez ce code avec votre téléphone intelligent pour accéder à la démo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>À faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur à scanner votre code QR.</b>Il n'y a rien de plus abstrait qu'un code QR sans indications. Imprimez votre code sur des feuilles ou des pochettes de CD et écrivez quelque chose comme :
				<ul>
					<li>"Scannez ce code pour entendre nos extraits</li>
					<li>"Écoutez nos chanson"</li>
				</ul>
				
				<li><b>Créez une page par événement.</b> Placez le code QR de la page de cet événement après y avoir ajouté vos extraits et l'heure et le lieu du concert.</li>
				
				<li><b>Gagnez de l'argent.</b> Placez-y un lien vers votre page iTunes pour vendre votre musique.</li>
				
				<li><b>Montez le son.</b> Téléversez vos chansons sur Soundcloud et intégrez-les dans votre page Qranberry, prêtes à être écoutées sur la plupart des téléphones intelligents.</li>
				
				<li><b>Sortez du placard.</b> Agrémentez votre site avec vos vidéos, entrevues, ou faites une vidéo spécialement pour vos visiteurs QR. Nous supportons les vidéos de YouTube, Vimeo, SocialCam, UStream, etc.</li>
				
			</ul>
		</section>
		
		<section>
			<h2>À ne pas faire</h2>
			<li><b>Ne surchargez pas votre page.</b> Nom du groupe, liens vers vos extraits, événements à venir, et profils sur les médias sociaux suffisent.</li>
			<li><b>Ne soyez pas trop volubile.</b> Coupez le bla bla, créez un site facile à lire avec des phrases courtes.</li>
			<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
		</section>
	</div>
</div>


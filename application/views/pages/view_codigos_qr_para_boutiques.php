<div class="page">
	<h1>Códigos QR para boutiques</h1>
	
	<div class="page_content">

		<section>
			<h2>Los códigos QR atraen más clientes a tu boutique</h2>
			<ul>
				<li>La vitrina de tu boutique atrae miradas, pero la gente no siempre tiene el tiempo de entrar. Deja que clientes potenciales accedan a tu sitio con un simple scan de código QR.</li>
				<li>Mantén informados a tus clientes de tus ventas y nuevos productos.</li>
				<li>Aumenta tu presencia en las redes sociales con una página móvil que hacer el compartir tan simple como el clic de un botón..</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/demo_boutique">Demo boutique</a><br>
				Escanéa esta código QR con tu móvil para acceder al demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_boutique.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_boutique&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Qué hacer</h2>
			<ul>
				<li><b>Da una razón para escanear.</b> Los códigos QR sufren de soledad si no tienen quien los escanee. Pega tu código QR en la ventana de tu boutique con algo como:
				<ul>
					<li>"Escanea para ver nuestras novedades"</li>
					<li>"Ventas y novedades, actualizadas cada semana"</li>
				</ul>
				
				<li><b>Una página es suficiente.</b> Una página actualizada regularmente puede ser suficiente para dar a tus clientes el gusto de visitar tu boutique. Muestra algunos de tus nuevos productos o artículos populares en oferta. Usa esto como un gancho, no como un inventario completo.</li>
				
				<li><b>Fotos, fotos, fotos</b> Los humanos somos creaturas visuales. Añade imágenes de tus productos para convertir a la gente que navega en clientes que compran.</li>

			</ul>
		</section>
		
		<section>
			<h2>Qué no hacer</h2>
			<ul>
				<li><b>No seas verboso.</b> Mantén el texto ligero, haciendo tu sitio fácil de leer sin largas frases o párrafos. Recuerda, la gente estará leyendo en un teléfono móvil.</li>
				<li><b>No lo pienses mucho.</b> No trates de tener un sitio perfecto en tu primer intento; sólo constrúyelo y diviértete. Si algo no funciona, podrás cambiarlo o añadir más ideas después.</li>
			</ul>
		</section>
	</div>
</div>


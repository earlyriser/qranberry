<div class="page">
	<h1>QR codes for restaurants</h1>
	
	<div class="page_content">

		<section>
			<h2>Benefits</h2>
			<ul>
				<li>Promote dishes which are difficult to describe by showing pictures of your restaurant items</li>
				<li>Decrease the missed expectations of clients who thought they were getting something different than what they imagined.</li>
				<li>Increase your social media reach with a mobile page that makes sharing as simple as a click of a button.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/alfredo">Restaurant demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> QR codes get lonely too, without any visitors. Add something next to your code, like:
				<ul>
					<li>"Scan this code to view pictures of our tasty recipes"</li>
					<li>"Not sure what to eat? Scan this"</li>
				</ul>
				<li><b>Create a mobile site (not just a mobile page).</b> Your homepage can link to the every section in your menu, especially if it is extensive</li>
				<li><b>Photos, photos, photos.</b> We know it's not easy to take photos of every dish, but the effort will be compensated by mouths salivating. Start with your house special or new recipes, and add some more when you have time.</li>
				<li><b>Nutritional insights.</b> If you have a nutritionist in your team and you know how many calories your dishes contain, add this information to allow customers to feel more comfortable with your food.</li>
				<li><b>Surprise your clients.</b> Make a fun video of the interior of your kitchen, or share a recipe, or show them where you buy your veggetables ... show your craft in a short video to make them feel right at home.</li>
				<li><b>Promote your menu of the day.</b> If your restaurant has a menu of the day, you can add a QR code in some of your street windows or boards, to make this information accessible to people working nearby.</li>
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Don't overcrowd your homepage.</b>Your restaurant name, a menu, and some contact information is sufficient.</li>
				<li><b>Don't be wordy.</b> Keep the text light, making your site easy to scan without long phrases or long paragraphs. Remember, QR viewers are viewing your content on a handheld phone.</li>
				<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot; just build it and have fun. If something doesn't work, you can change or add new ideas later.</li>
			</ul>
		</section>
	</div>
</div>


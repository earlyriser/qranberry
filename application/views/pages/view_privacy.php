<div class="page">
	<h1>Privacy Policy</h1>
	
	<div class="page_content">

		<p>		
			By using or visiting this website, you signify your agreement to this Privacy Policy and our Terms of Service. These documents constitute an agreement between Réseau TG SENC (“Qranberry”) and you. The terms “we”, “us” and “our” refer to Qranberry.
		</p>

		<h2>INFORMATION WE COLLECT</h2>

		<p>
		The users of Qranberry are visitors or members. A member is a user who provided specific types of  information to create an account and use the site’s features. The information collected from members and visitors can include their Internet Protocol address, site browsing patterns, the browser used to access Qranberry and other computer details, and site use.
		Your email address is collected as part of registration and will remain confidential. We will neither sell this information nor share it with outside parties.
		</p>

		<h2>SHARING INFORMATION</h2>

		<p>
		Our members’ private information shall not be divulged to anyone, except when expressly ordered by a court of law, or when we believe that disclosure is necessary in order to protect our rights and/or comply with a judicial proceeding, court order or legal process served on our Web site.
		We may share information with third parties (including, but not limited to, our online advertisers) pertaining to how certain groups of individuals use the site, but never information on individual users.
		</p>

		<h2>COOKIES</h2>

		<p>
		Cookies are simple text files that are placed on your computer to store specific pieces of information. We use cookies to remember your personal preferences and customize your experience while visiting our site. Third-party cookies (such as those from Google Analytics or Adsense) may be found on our site. We exercise no control over such cookies.
		</p>


		<h2>CHILDREN</h2>

		<p>
		Because all members must be at least thirteen years old, we do not collect any information on individuals less than thirteen years of age.
		</p>

		<h2>MODIFICATIONS</h2>

		<p>
		We reserve the right to modify this Privacy Policy at any time, at our sole discretion, and without prior notice. Continuing to use the site after such changes have been made implicitly indicates your acceptance thereof.
		</p>		
	</div>
</div>


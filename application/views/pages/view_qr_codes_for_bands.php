<div class="page">
	<h1>QR codes for bands</h1>
	
	<div class="page_content">

		<section>
			<h2>Benefits</h2>
			<ul>
				<li>Get more fans by giving a glimpse of your music through flyers and CD covers</li>
				<li>Increase your social media reach with a mobile page that makes the sharing as simple as the click of a button.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="">Band demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> There is nothing more lonely in the world than a QR code without any indication. Print your code in your flyers or CD covers and write something like 
				<ul>
					<li>"Scan this code to hear our tunes"</li>
					<li>"Hear our songs"</li>
				</ul>
				
				<li><b>Create a page per event.</b> Put the page QR code on the flyers of this event and in your page include some songs (extracts if you prefer), venue and time.</li>
				
				<li><b>Get some money.</b> Put a link to your iTunes page or the place you sell your music</li>
				
				<li><b>Pump up the volume.</b> Upload your songs on Soundcloud and embed them in your Qranberry page, ready to be heared in practically any smartphone.</li>
				
				<li><b>Video killed the radio star?</b> Spice your site with some of your videos, interviews or just make a special video for your qr visitors. We accept videos from YouTube, Vimeo, SocialCam, UStream and more.</li>
				
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<li><b>Don't overcrowd your homepage.</b>Your band's name, links to your songs, future shows and social media profiles is enough.</li>
			<li><b>Don't be expansive.</b> Cut the blablabla, make your site easy to scan supressing the long phrases.</li>
			<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot, just built it and have fun. If something doesn't work you can change it later.</li>
		</section>
	</div>
</div>


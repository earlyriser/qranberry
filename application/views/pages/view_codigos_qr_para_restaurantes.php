<div class="page">
	<h1>Códigos QR para restaurantes</h1>
	
	<div class="page_content">

		<section>
			<h2>Beneficios</h2>
			<ul>
				<li>Promueve platillos que son difíciles de imaginar a partir de un simple texto en tu menú al mostrar fotos de ellos en tu página móvil</li>
				<li>Elimina las confusiones de tus clientes que piensan pedir un platillo y lo que reciben no corresponde a lo que habían imaginado</li>
				<li>Aumenta tu presencia en las redes sociales con una página móvil que hacer el compartir tan simple como el clic de un botón.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/alfredo">Demo restaurant</a><br>
				Escanéa esta código QR con tu móvil para acceder al demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>
		</section>
		
		<section>
			<h2>Qué hacer</h2>
			<ul>
				<li><b>Da una razón para escanear tu código QR</b> Los códigos QR sufren de soledad sin visitantes. Añade junto a tu código algo como:
				<ul>
					<li>"Escanea para ver fotos de nuestros ricos platillos."</li>
					<li>"¿No sabes qué pedir? Escanea este código."</li>
				</ul>
				<li><b>Crea un sitio móvil (no sólo una página)</b> Tu página de inicio puede enlazar a cada parte de tu menú (ensaladas, sopas, postres...), sobretodo si ofreces una gran variedad.</li>
				<li><b>Fotos, fotos, fotos.</b> Sabemos que no es fácil tomar fotos de cada platillo, pero el esfuerzo sera recompensado con bocas hechas agua. Empieza con las especialidades de la casa y ve añadiendo más cuando tengas tiempo.</li>
				<li><b>Información nutricional.</b> Si tienes un Nutriólogo en tu equipo y sabes cuantas calorías contienen tus platos, añade esta información para permitir a tus clientes saber más sobre tu comida.</li>
				<li><b>Sorprende.</b> Has un video del interio de la cosina, comparte un receta o muestra dónde compras las verduras... Muestra tu arte en un corto (menos de 90 segundos) video para hacer a tus clientes como en casa.</li>
				<li><b>Menú del día.</b> Si tu restaurant ofrece un menú del día, puedes pegar un código QR en tus ventanas para hacer esta información accesible a la gente que trabaja cerca.</li>
			</ul>
		</section>
		
		<section>
			<h2>Qué no hacer</h2>
			<ul>
				<li><b>No satures tu página de inicio.</b>El nombre de tu restaurant, un menú de navigación y la información de contacto es suficiente.</li>
				<li><b>No seas verboso.</b> Mantén el texto ligero, haciendo tu sitio fácil de leer sin largas frases o párrafos. Recuerda, la gente estará leyendo en un teléfono móvil.</li>
				<li><b>No lo pienses mucho.</b> No trates de tener un sitio perfecto en tu primer intento; sólo constrúyelo y diviértete. Si algo no funciona, podrás cambiarlo o añadir más ideas después.</li>
			</ul>
		</section>
	</div>
</div>


<div class="page">
	<h1>Terms of Use</h1>
	
	<div class="page_content">

		<h2>1. YOUR ACCEPTANCE</h2>
		<p>
			The purpose of this website,  www.qranberry.com (the “Site”), owned  and operated by Réseau TG SENC ("We", "Qranberry"), is to  provide web publishing  services. Please read these terms of  service  (“Agreement”) carefully before using the Site or any services  provided  on the Site (collectively, “Services”).  By using or accessing  the  Services, you agree to become bound by all the terms and  conditions of  this Agreement.  If you do not agree to all the terms and  conditions of  this Agreement, do not use the Services.  The Services  are accessed by  You (“Subscriber” or “You”) under the following terms  and conditions:
		</p>
				
		<h2>2. MODIFICATIONS TO THIS AGREEMENT</h2>
		<p>
			We reserve the right, at our sole discretion, to change, modify or otherwise alter these terms and conditions at any time.  Such modifications shall become effective immediately upon the posting thereof. You must review this agreement on a regular basis to keep yourself apprised of any changes.
		</p>

		<h2>3. ACCESS</h2>
		<p>
		You must be at least thirteen years of age in order to use this  site. It is your responsibility to ensure that the information you  provide during registration is accurate, and you must update your information so it becomes accurate once again in the event that your  information changes.
		</p>
				
		<p>We will use reasonable efforts to ensure that the Site and Services are available twenty-four hours a day, seven days a week.  However,  there will be occasions when the Site and/or Services will be  interrupted for maintenance, upgrades and repairs or due to failure of  telecommunications links and equipment.  Every reasonable step will be  taken by us to minimize such disruption where it is within our reasonable control.
		</p>

		<p>
		You agree that neither Qranberry nor the Site will be liable in any event  to you or any other party for any suspension, modification,  discontinuance or lack of availability of the Site, the service, your  Subscriber Content or other Content. You acknowledge that Qranberry may establish limits concerning use of the Service, including the maximum number of days that Subscriber Content will be retained by the Service, the maximum number and size of postings, or other Content that may be transmitted or stored by the Service, and the frequency with which you may access the Service.
		</p>

		<h2>4. CONTENT</h2>
		<p>
			The Site and its contents are intended solely for the use of Qranberry Subscribers and may only be used in accordance with the terms of this  Agreement.  All materials displayed or performed on the Site, (collectively, “Content”) (other than  Content posted by Subscriber (“Subscriber Content”)) are the property of Qranberry and/or third parties and are protected by Canada and  international copyright laws.  All trademarks,  service marks, and trade names are proprietary to Qranberry and/or third  parties.  Subscriber shall abide by all copyright notices, information,  and restrictions contained in any Content accessed through the Services.
		</p>
		<p>        
			Subscriber may download or copy the Content, and other items  displayed on the Site for download, for personal use only, provided that  Subscriber maintains all copyright and other notices contained in such  Content.  Downloading, copying, or storing any Content for other than  personal, noncommercial use is expressly prohibited without prior  written permission from Qranberry, or from the copyright holder identified  in such Content's copyright notice.
		</p>

		<h2>5. SUBSCRIBER CONTENT</h2>
		<p>
		Subscriber shall own all Subscriber Content  that Subscriber contributes  to the Site, but hereby grants and agrees to grant Qranberry a  non-exclusive, worldwide, royalty-free, transferable right and license  (with the right to sublicense), to use, copy, cache, publish, display,  distribute and store such Subscriber Content and to allow others in order to  provide the Services.  On termination of Subscriber’s membership to the  Site and use of the Services, Qranberry shall make all reasonable efforts  to promptly remove from the Site and cease use of the Subscriber  Content; however, Subscriber recognizes and agrees that caching of or  references to the Subscriber Content may not be immediately removed.   Subscriber warrants, represents and agrees Subscriber has the right to  grant Qranberry and the Site the rights set forth above.  Subscriber  represents, warrants and agrees that it will not contribute any  Subscriber Content that <br>
		a) infringes, violates or otherwise interferes  with any copyright or trademark of another party<br> 
		b) reveals any trade  secret, unless Subscriber owns the trade secret or has the owner’s  permission to post it<br> 
		c) infringes any intellectual property right of  another or the privacy or publicity rights of another<br>
		d) is libelous,  defamatory, abusive, threatening, harassing, hateful, offensive or  otherwise violates any law or right of any third party<br>
		e) contains a  virus, trojan horse, worm, time bomb or other computer programming  routine or engine that is intended to damage, detrimentally interfere  with, surreptitiously intercept or expropriate any system, data or  information<br>
		f) remains posted after Subscriber has been notified  that such Subscriber Content violates any of sections (a) to (e) of this  point.  
		</p>
		<p>
		Qranberry reserves the right to remove any Subscriber Content  from the Site, suspend or terminate Subscriber’s right to use the  Services at any time, or pursue any other remedy or relief available to  Qranberry and/or the Site under equity or law, for any reason.You understand that all Subscriber Content posted on, transmitted through, or linked from the Service, are the sole responsibility of the person from whom such Subscriber Content originated. More specifically, you are entirely responsible for each individual item ("Item") of Content that you post, email or otherwise make available via the Service. You understand that Qranberry does not control, and is not responsible for Subscriber Content made available through the Service, and that by using the Service, you may be exposed to Subscriber Content that is offensive, indecent, inaccurate, misleading, or otherwise objectionable.  Qranberry shall have the right (but not the obligation) in its sole discretion to refuse, delete or move any Content that is available via the Service, for violating the letter or spirit of the TOU or for any other reason.
		</p>

		<h2>6. LINKS</h2>
		<p>
		The Site available through the Service may contain links to other websites, which are completely independent of Qranberry.  Qranberry makes no representation or warranty as to the accuracy, completeness or authenticity of the information contained in any such site.  Your linking to any other webites is at your own risk. 
		</p>

		<h2>7.  NOTIFICATION OF CLAIMS OF INFRINGEMENT</h2>
		<p>
		If you believe that your work has been copied in a way that constitutes 
		copyright infringement, or your intellectual property rights have been 
		otherwise violated, please notify Qranberry at info@qranberry.me with the following Notice:<br>
		a) Identify the material on the Qranberry site that you claim is infringing, with enough detail so that we may locate it on the website.<br>
		b) A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law.<br>
		c) A statement by you declaring under penalty of perjury that (1) the above information in your Notice is accurate, and (2) that you are the owner of the copyright interest involved or that you are authorized to act on behalf of that owner.<br>
		d) Your address, telephone number, and email address.<br>
		e) Your physical or electronic signature.<br>
		Qranberry will remove the infringing Subscriber Content.
		</p>

		<h2>8. PRIVACY AND INFORMATION DISCLOSURE</h2>
		<p>
		Qranberry has established a Privacy Policy to explain to users how their information is collected and used, which is located at the following web address:  http://qranberry.com/privacy
		</p>

		<h2>9. INDEMNITY</h2>
		<p>
		You agree to indemnify and hold Qranberry, its officers, subsidiaries, affiliates, successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless from any claim or demand, including reasonable attorney fees and court costs, made by any third party due to or arising out of Subscriber Content you submit, post or make available through the Service, your use of the Service, your violation of the TOU, your breach of any of the representations and warranties herein, or your violation of any rights of another.
		</p>

		<h2>10. DISCLAIMER OF WARRANTIES</h2>
		<p>
		YOUR USE OF THE SITE AND ITS SERVICES ARE AT YOUR SOLE RESPONSIBILITY AND RISK. THE SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. QRANBERRY EXPRESSLY DISCLAIMS ALL REPRESENTATIONS, WARRANTIES, OR CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY AND NON-INFRINGEMENT. NEITHER QRANBERRY MAKES ANY WARRANTY THAT THE CONTENT OR SUBSCRIBER CONTENT WILL MEET YOUR REQUIREMENTS OR BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE.
		</p>

		<h2>11. LIMITATIONS OF LIABILITY</h2>
		<p>
		UNDER NO CIRCUMSTANCES SHALL QRANBERRY BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES (EVEN IF QRANBERRY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM ANY ASPECT OF YOUR USE OF THE QRANBERRY SITE OR THE SERVICE, WHETHER THE DAMAGES ARISE FROM USE OR MISUSE OF THE QRANBERRY SITE OR THE SERVICE, FROM INABILITY TO USE THE QRANBERRY SITE OR THE SERVICE, OR THE INTERRUPTION, SUSPENSION, MODIFICATION, ALTERATION, OR TERMINATION OF THE QRANBERRY SITE OR THE SERVICE.  SUCH LIMITATION SHALL ALSO APPLY WITH RESPECT TO DAMAGES INCURRED BY REASON OF OTHER SERVICES OR PRODUCTS RECEIVED THROUGH OR ADVERTISED IN CONNECTION WITH THE QRANBERRY SITE OR THE SERVICE OR ANY LINKS ON THE QRANBERRY SITE, AS WELL AS BY REASON OF ANY INFORMATION OR ADVICE RECEIVED THROUGH OR ADVERTISED IN CONNECTION WITH THE QRANBERRY SITE OR THE SERVICE OR ANY LINKS ON THE QRANBERRY SITE.  THESE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW. In some jurisdictions, limitations of liability are not permitted.  In such jurisdictions, some of the foregoing limitation may not apply to you.
		</p>

		<h2>12. FEES AND PAYMENT</h2>
		<p>
		Some of the Services require payment of fees.  All fees are  stated in U.S. dollars.  Subscriber shall pay all applicable fees, as  described on the Site in connection with such Services selected by  Subscriber.  All fees are  non-refundable unless expressly stated otherwise on the Site. Subscriber represents to Qranberry that Subscriber is the authorized  account holder or an authorized user of the chosen method of payment  used to pay for the paid aspects of the Services. All fee-based Services  and virtual goods are provided “AS IS” with no warranties of any kind.  Qranberry may modify and/or eliminate such fee-based Services at its  discretion. Qranberry may change its prices at any time but will provide you  reasonable notice of any such changes by posting the new prices on the  Site and by sending you email notification. If you do not wish to pay  the new prices, you may cancel the services prior to the change going  into effect.
		</p>

		<h2>13. TERMINATION</h2>
		<p>
		You agree that Qranberry, in its sole discretion, has the right (but not the obligation) to delete or deactivate your account, block your email or IP address, or otherwise terminate your access to or use of the Service (or any part thereof), immediately and without notice, and remove and discard any Subscriber Content within the Service, for any reason, including, without limitation, if Qranberry believes that you have acted inconsistently with the letter or spirit of the TOU. Further, you agree that Qranberry shall not be liable to you or any third-party for any termination of your access to the Service.  Further, you agree not to attempt to use the Service after said termination.  
		</p>

	</div>
</div>


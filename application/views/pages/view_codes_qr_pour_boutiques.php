<div class="page">
	<h1>Codes QR pour boutiques</h1>
	
	<div class="page_content">

		<section>
			<h2>Les codes QR peuvent amener plus de clients � votre boutique</h2>
			<ul>
				<li>Votre vitrine unique capte leur regard mais ils n'ont pas le temps d'arr�ter? Ne les laissez pas oublier! Permettez � vos clients potentiels d'ajouter facilement votre page � leurs favoris avec un code QR.</li>
				<li>Informez vos clients r�guliers de vos soldes et nouveaux arrivages.</li>
				<li>Partez � l'assaut des m�dias sociaux avec une page mobile qui rend le partage facile (un clic de bouton).</li>
			</ul>
		</section>
		
		<section>
			<h2>D�mo</h2>
			<p>
				<a href="http://qranberry.me/demo_boutique">D�mo boutique</a><br>
				Scannez ce code avec votre t�l�phone intelligent pour acc�der � la d�mo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_boutique.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_boutique&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>� faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur � scanner votre code QR.</b> Sans visiteurs, les codes ne servent � rien. Placez-le sur votre vitrine ou pr�s du trottoir, avec des indications comme : 
				<ul>
					<li>"Scannez ce code pour voir nos nouveaux arrivages"</li>
					<li>"Soldes et nouveaux arrivages, mis � jour � chaque semaine"</li>
				</ul>
				
				<li><b>Une seule page suffit</b> Une page � jour suffit � g�rer la quantit� d'informations dont vous avez besoin pour attirer vos clients. Mettez en vedette quelques nouveaut�s et articles populaires.  Le but est d'attirer et non d'afficher toute votre liste d'inventaire.</li>
				
				<li><b>Photos, photos, photos.</b> Les humains sont des cr�atures visuelles. Ajouter des photos de vos articles peut convertir les magasineurs en acheteurs.</li>

			</ul>
		</section>
		
		<section>
			<h2>� ne pas faire</h2>
			<ul>
				<li><b>Ne soyez pas trop volubile.</b> Votre site doit demeurer l�ger, facile � scanner, avec des phrases et paragraphes courts. Gardez � l'esprit que les utilisateurs regardent votre contenu sur un t�l�phone portable.</li>
				<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
			</ul>
		</section>
	</div>
</div>


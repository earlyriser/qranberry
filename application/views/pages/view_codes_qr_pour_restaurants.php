<div class="page">
	<h1>Codes QR pour restaurants</h1>
	
	<div class="page_content">

		<section>
			<h2>Avantages</h2>
			<ul>
				<li>Faites la promotion de plats difficiles à décrire en affichant des photos.</li>
				<li>Évitez la déception des clients qui reçoivent un plat différent de celui qu'ils avaient imaginé. </li>
				<li>Partez à l'assaut des médias sociaux avec une page mobile qui rend le partage facile (un clic de bouton).</li>
			</ul>
		</section>
		
		<section>
			<h2>Démo</h2>
			<p>
				<a href="http://qranberry.me/alfredo">Démo restaurants</a><br>
				Scannez ce code avec votre téléphone intelligent pour accéder à la démo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>
		</section>
		
		<section>
			<h2>À faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur à scanner votre code QR.</b> Sans visiteurs, les codes ne servent à rien. Ajoutez-y une phrase du genre :
				<ul>
					<li>"Scannez ce code pour voir des images de nos plats savoureux"</li>
					<li>"Incapable de choisir ? Scannez ceci :"</li>
				</ul>
				<li><b>Créez un site mobile (pas seulement une page mobile).</b> Votre page d'accueil peut faire des liens vers toutes les sections de votre menu, surtout s'il est volumineux.</li>
				<li><b>Photos, photos, photos.</b> Nous savons qu'il n'est pas facile de prendre des photos de chaque plat, mais l'effort sera récompensé par les bouches qui salivent. Commencez par vos recettes maison ou vos nouveautés, et ajoutez-en d'autres quand vous en avez le temps.</li>
				<li><b>Informations nutritionnelles.</b> Si vous servez des plats santé et que vous connaissez le nombre de calories de vos plats, ajoutez ces informations pour permettre à vos clients de manger sans culpabilité.</li>
				<li><b>Surprenez vos clients.</b> Faites une vidéo amusante de l'intérieur de votre cuisine, partagez une recette ou montrez-leur où vous achetez vos légumes... Faites-les sentir comme à la maison.</li>
				<li><b>Faites la promotion de votre menu du jour.</b> Si votre restaurant propose un menu du jour, vous pouvez ajouter un code QR dans votre vitrine ou ailleurs, et rendre cette information accessible aux personnes qui travaillent à proximité.</li>
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Ne surchargez pas votre page.</b>Le nom de votre restaurant, le menu et vos coordonnées suffisent.</li>
				<li><b>Ne soyez pas trop volubile.</b> Votre site doit demeurer léger, facile à scanner, avec des phrases et paragraphes courts. Gardez à l'esprit que les utilisateurs regardent votre contenu sur un téléphone portable.</li>
				<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
			</ul>
		</section>
	</div>
</div>


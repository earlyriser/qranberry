<div class="page">
	<h1>Codes QR pour l'immobilier</h1>
	
	<div class="page_content">
		
		<section>
			<h2>Avantages</h2>
			<ul>
				<li>Gagnez du temps en limitant les appels qu'une simple page d'information peut résoudre. Le panneau "À louer" typique ne contient pas suffisamment d'information en lui-même, mais si vous lui ajoutez un code QR, les gens peuvent accéder à une page contenant toute les informations sur la propriété et d'obtenir des réponses à leurs questions, par exemple à savoir si leur chat sera accepté.</li>
				<li>Filtrez vos visiteurs en incluant des photos ou même une vidéo de votre propriété sur votre page. De cette façon, seuls les acheteurs sérieux et informés prendront contact avec vous.</li>
				<li>Rejoignez plus de personnes. Chercher un numéro de téléphone et faire un appel gaspille le temps que les gens très occupés sur la rue n'ont pas. Offrez-leur la facilité d'un simple balayage pour obtenir l'information dont ils ont besoin.</li>
			</ul>
		</section>
		
		<section>
			<h2>Démo</h2>
			<p>
				<a href="http://qranberry.me/demo_inmo">Démo immobilier</a><br>
				Scannez ce code avec votre téléphone intelligent pour accéder à la démo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_real_state.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_inmo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>À faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur à scanner votre code QR.</b> Placez votre code sur votre signe (panneau) et écrivez quelque chose comme :
				<ul>
					<li>"Scannez pour obtenir plus d'informations"</li>
					<li>"Photos et prix"</li>
				</ul>

				<li><b>Faites la promotion de toutes vos propriétés.</b> Si vous êtes un agent immobilier, vous pouvez créer une page d'accueil incluant des liens vers l'ensemble de vos propriétés. Ces visiteurs peuvent devenir des acheteurs potentiels d'une autre de vos propriétés et non seulement de celle qu'ils ont vue.</li>
				
				<li><b>Ajoutez une visite en vidéo.</b> Installez SocialCam sur votre téléphone intelligent, le cinéma mobile, filmez la propriété prèce par pièce, et intégrez la vidéo sur votre page. Rien de tel qu'une vidéo pour transmettre l'ambiance d'un appartement ou une maison.</li>
			</ul>
		</section>
		
		<section>
			<h2>À ne pas faire</h2>
			<ul>
				<li><b>Ne soyez pas trop volubile.</b> Votre site doit demeurer léger, facile à scanner, avec des phrases et paragraphes courts. Gardez à l'esprit que les utilisateurs regardent votre contenu sur un téléphone portable.</li>
				<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
			</ul>
		</section>
	</div>
</div>


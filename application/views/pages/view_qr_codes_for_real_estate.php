<div class="page">
	<h1>QR codes for real estate</h1>
	
	<div class="page_content">
		
		<section>
			<h2>Benefits</h2>
			<ul>
				<li>Save time by avoiding calls that a simple page of information can solve. The typical "For rent" sign doesn't relay enough information by itself, but if you add a QR code to your sign, people can access a page of information all about the property and get basic questions answered, like if their cats are accepted.</li>
				<li>Filter your visitors by showing photos or even a video of your property on your page. This way, only the serious and educated buyers will contact you.</li>
				<li>Reach more people. Looking for a phone number and making a call takes time that the busiest people on the street don't have. Give them the convenience of a simple scan to get the information they need.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/demo_inmo">Real estate demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_real_estate.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_inmo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> Paste your code on your sign and write something like:
				<ul>
					<li>"Scan to get more info"</li>
					<li>"Photos and price"</li>
				</ul>

				<li><b>Cross-promote your properties.</b> If you are a real estate agent, you can make a homepage that links to all of your properties. Once you get a visitor, they'll be a potential buyer for all of the properties, rather than just the one they initially saw.</li>
				
				<li><b>Include a video tour.</b> Install SocialCam on your mobile phone, film a property tour, and embed it on your page. There's nothing better than a video to display the feel of an apartment/house.</li>
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Don't be wordy.</b> Keep the text light, making your site easy to scan without long phrases or long paragraphs. Remember, QR viewers are viewing your content on a handheld phone.</li>
				<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot; just build it and have fun. If something doesn't work, you can change or add new ideas later.</li>
			</ul>
		</section>
	</div>
</div>


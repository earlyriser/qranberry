<div class="page">
	<h1>QR codes for products</h1>
	
	<div class="page_content">

		<section>
			<h2>Benefits</h2>
			<ul>
				<li>An indecisive customer has been standing in the middle of a supermarket aisle, deliberating your product. Persuade them on the spot with a video or more information embedded on your page.</li>
				<li>Conveniently give your customer all the information they need to use your product, and allow them to save it to access again and again later.</li>
				<li>Increase your social media reach with a mobile page that makes sharing as simple as a click of a button.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="">Product demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> Print your code on your container and write something like 
				<ul>
					<li>"See how we craft this beer"</li>
					<li>"Scan to watch our best recipes"</li>
					<li>"How to take care of your plant"</li>
					<li>"Watch how our eco diapers are as good as the popular brand"</li>
				</ul>
				
				<li><b>Different page per product.</b> Have lots of products? Give each one its own page, but keep the information concise.</li>
				
				<li><b>Build a nice homepage.</b> Create a navigational menu with links to all your products and your social media profiles.</li>
				
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Don't be wordy.</b> Keep the text light, making your site easy to scan without long phrases or long paragraphs. Remember, QR viewers are viewing your content on a handheld phone.</li>
				<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot; just build it and have fun. If something doesn't work, you can change or add new ideas later.</li>
			</ul>
		</section>
	</div>
</div>


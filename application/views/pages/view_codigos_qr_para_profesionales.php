<div class="page">
	<h1>Códigos QR para profesionales</h1>
	
	<div class="page_content">

		<section>
			<h2>Beneficios</h2>
			<ul>
				<li>Comparte tu CV fácilmente gracias a un código QR en tu tarjeta de presentación.</li>
				<li>Muestra tu portafolio o realizaciones</li>
				<li>Aumenta tu presencia en las redes sociales con una página móvil que hacer el compartir tan simple como el clic de un botón.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/roberto">Demo profesional</a><br>
				Escanéa esta código QR con tu móvil para acceder al demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_professional.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/roberto&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Qué hacer</h2>
			<ul>
				<li><b>Da una razón para escanear tu código QR.</b> Imprime el código en tu tarjeta o CV con algo como: 
				<ul>
					<li>"Vea mis realizaciones"</li>
					<li>"Proyectos pasados"</li>
				</ul>
				
				<li><b>Construye un buen sitio.</b> Tu página de aterrizaje (la página donde la gente llega después de haber escaneado tu código QR) puede ser la página de inicio o la página de tus realizaciones. Una página de inicio incluye un menú de navegación con enlaces a tus realizaciones, CV y perfiles de Twitter y LinkedIn, además de la información de contacto y una breve biografía. La página de realizaciones debe incluir imágenes y texto que muestren tu experiencia y habilidades</li>

				<li><b>Consigue el trabajo.</b> Si estás aplicando para un trabajo, puedes diseñar la página de aterrizaje específicamente para esta ocasión y añadir un video explicando porqué deseas trabajar ahí. Esto te hará memorable.</li>
			</ul>
		</section>
		
		<section>
			<h2>Qué no hacer</h2>
			<ul>
				<li><b>No seas verboso.</b> Mantén el texto ligero, haciendo tu sitio fácil de leer sin largas frases o párrafos. Recuerda, la gente estará leyendo en un teléfono móvil.</li>
				<li><b>No lo pienses mucho.</b> No trates de tener un sitio perfecto en tu primer intento; sólo constrúyelo y diviértete. Si algo no funciona, podrás cambiarlo o añadir más ideas después.</li>
			</ul>
		</section>
	</div>
</div>


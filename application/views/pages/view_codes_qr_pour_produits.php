<div class="page">
	<h1>Codes QR pour produits</h1>
	
	<div class="page_content">

		<section>
			<h2>Avantages</h2>
			<ul>
				<li>Un client indécis est en train d'évaluer votre produit au supermarché. Persuadez-le sur le champ avec une vidéo ou plus d'informations, intégrées à votre page.</li>
				<li>Informez votre client sur l'utilisation de votre produit et permettez-lui de l'enregistrer pour y accéder à nouveau, tant qu'il le voudra.</li>
				<li>Partez à l'assaut des médias sociaux avec une page mobile qui rend le partage facile (un clic de bouton).</li>
			</ul>
		</section>
		
		<section>
			<h2>Démo</h2>
			<p>
				<a href="">Démo produit</a><br>
				Scannez ce code avec votre téléphone intelligent pour accéder à la démo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_restaurant.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/alfredo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>À faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur à scanner votre code QR.</b>Imprimez le code sur le contenant et écrivez quelque chose comme :
				<ul>
					<li>"Voyez comment nous fabriquons cette bière"</li>
					<li>"Scannez pour voir nos meilleures recettes"</li>
					<li>"Comment prendre soin de cette plante"</li>
					<li>"Découvrez comment nos couches écologiques sont aussi bonnes que la marque populaire</li>
				</ul>
				
				<li><b>Une page différente par produit</b> Vous avez de nombreux produits? Faites une page pour chacun, mais l'information doit demeurer concise.</li>
				
				<li><b>Faites une belle page d'accueil</b> Créez un menu de navigation avec des liens vers tous vos produits et vers vos profils sur les médias sociaux.</li>
				
			</ul>
		</section>
		
		<section>
			<h2>À ne pas faire</h2>
			<ul>
				<li><b>Ne soyez pas trop volubile.</b> Votre site doit demeurer léger, facile à scanner, avec des phrases et paragraphes courts. Gardez à l'esprit que les utilisateurs regardent votre contenu sur un téléphone portable.</li>
				<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
			</ul>
		</section>
	</div>
</div>


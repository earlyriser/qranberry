<div class="page">
	<h1>Codes QR pour les professionnels</h1>
	
	<div class="page_content">

		<section>
			<h2>Avantages</h2>
			<ul>
				<li>Partagez votre CV facilement et rapidement grâce à un code QR sur votre curriculum vitae ou votre carte de visite.</li>
				<li>Présentez votre portfolio et vos projets</li>
				<li>Partez à l'assaut des médias sociaux avec une page mobile qui rend le partage facile (un clic de bouton).</li>
			</ul>
		</section>
		
		<section>
			<h2>Démo</h2>
			<p>
				<a href="http://qranberry.me/roberto">Démo site pour professionnel</a><br>
				Scannez ce code avec votre téléphone intelligent pour accéder à la démo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_professional.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/roberto&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>À faire</h2>
			<ul>
				<li><b>Incitez l'utilisateur à scanner votre code QR.</b> Imprimez votre code sur votre carte de visite ou votre CV et écrivez quelque chose comme : 
				<ul>
					<li>"Voir mon portfolio"</li>
					<li>"Projets antérieurs"</li>
				</ul>
				
				<li><b>Construisez un site attrayant</b> Votre page de destination (la page vers laquelle sont dirigés les utilisateurs en scannant le code QR) pourrait être une page d'accueil ou un portfolio. Une page d'accueil inclure un menu de navigation avec des liens vers votre portfolio, CV, profils sur Likedin et Twitter, coordonnées et une brève biographie. La page de votre portfolio pourrait inclure des images et du texte qui démontrent vos réalisations et compétences.</li>

				<li><b>Obtenez l'emploi.</b> Si vous postulez un emploi, vous pouvez faire une page de destination spécialement conçue pour cette occasion, et ajouter une vidéo expliquant pourquoi vous souhaitez obtenir ce poste. On ne pourra pas vous oublier!</li>
			</ul>
		</section>
		
		<section>
			<h2>À ne pas faire</h2>
			<ul>
				<li><b>Ne soyez pas trop volubile.</b> Votre site doit demeurer léger, facile à scanner, avec des phrases et paragraphes courts. Gardez à l'esprit que les utilisateurs regardent votre contenu sur un téléphone portable.</li>
				<li><b>N'y pensez pas trop.</b> N'essayez pas d'avoir un site parfait au premier essai. Faites-le simplement en ayant du plaisir. Vous pourrez toujours apporter des changements plus tard.</li>
			</ul>
		</section>
	</div>
</div>


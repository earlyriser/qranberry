<div class="page">
	<h1>QR codes for boutiques</h1>
	
	<div class="page_content">

		<section>
			<h2>QR codes can attract more customers to your boutique</h2>
			<ul>
				<li>Your unique storefront caught their eye, but they don't have time to stop by. Don't let them forget! Let potential customers bookmark your store quickly with the scan of a QR code.</li>
				<li>Keep your loyal customers informed about sales and new arrivals.</li>
				<li>Increase your social media reach with a mobile page that makes sharing as simple as a click of a button.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/demo_boutique">Boutique demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_boutique.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_boutique&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> QR codes get lonely too, without any visitors. Stick your code on your front window or sidewalk board, with something like:
				<ul>
					<li>"Scan this code to access our new arrivals"</li>
					<li>"Sales and new arrivals, updated weekly"</li>
				</ul>
				
				<li><b>A page is enough.</b> One updated page can handle just the amount of information you need to entice your customers. Show some of your new arrivals and popular items on sale. Use this as a hook, rather than as an inventory list.</li>
				
				<li><b>Photos, photos, photos.</b> Humans are visual creatures. Add pictures of your items to convert people that are browsing into people that will shop.</li>

			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Don't be wordy.</b> Keep the text light, making your site easy to scan without long phrases or long paragraphs. Remember, QR viewers are viewing your content on a handheld phone.</li>
				<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot; just build it and have fun. If something doesn't work, you can change or add new ideas later.</li>
			</ul>
		</section>
	</div>
</div>


<div class="page">
	<h1>Códigos QR para inmobiliarias</h1>
	
	<div class="page_content">
		
		<section>
			<h2>Beneficios</h2>
			<ul>
				<li>Ahorra tiempo evitando llamadas telefónicas que una simple página móvil puede responder. El típico signo "En renta" no da mucha información, pero si le añades un código QR la gente puede acceder a una página con toda la información sobre la propiedad.</li>
				<li>Filtra las llamadas al mostrar fotos y videos de la propiedad en tu página. De esta manera sólo la gente realmente interesade te contactará.</li>
				<li>Alcanza a más gente. Anotar un teléfono y hacer una llamada a partir de un anuncio en la calle toma tiempo que las personas más ocupadas no tienen. Dales la conveniencia de un simple escán para ofrecerles toda la información que necesitan.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/demo_inmo">Demo inmobiliario</a><br>
				Escanéa esta código QR con tu móvil para acceder al demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_real_state.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/demo_inmo&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Qué hacer</h2>
			<ul>
				<li><b>Da una razón para escanear tu código QR</b> Pega tu código en tu anuncio acompañado de algo como:
				<ul>
					<li>"Obtén más información al escanear"</li>
					<li>"Fotos y precio"</li>
				</ul>

				<li><b>Promueve todas tus propiedades</b> Si eres un agente de bienes raíces, puedes hacer una página de inicio con enlaces a todos tus inmuebles. Una vez que tienes visitas, ellos puedes ser clientes potenciales para tus otros inmuebles si el que visitan no les interesa.</li>
				
				<li><b>Incluye una visita con video</b> Filma un tour de la propiedad y añádelo en tu página móvil (puedes usar Youtube o SocialCam). No hay nada mejor que un video para dar una buena ideas de las dimensiones de una casa o departamento.</li>
			</ul>
		</section>
		
		<section>
			<h2>Qué no hacer</h2>
			<ul>
				<li><b>No seas verboso.</b> Mantén el texto ligero, haciendo tu sitio fácil de leer sin largas frases o párrafos. Recuerda, la gente estará leyendo en un teléfono móvil.</li>
				<li><b>No lo pienses mucho.</b> No trates de tener un sitio perfecto en tu primer intento; sólo constrúyelo y diviértete. Si algo no funciona, podrás cambiarlo o añadir más ideas después.</li>
			</ul>
		</section>
	</div>
</div>


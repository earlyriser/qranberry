<div class="page">
	<h1>QR codes for professionals</h1>
	
	<div class="page_content">

		<section>
			<h2>Benefits</h2>
			<ul>
				<li>Easy and quickly share your CV through a QR code in your resume or presentation card</li>
				<li>Showcase your portfolio and projects</li>
				<li>Increase your social media reach with a mobile page that makes sharing as simple as a click of a button.</li>
			</ul>
		</section>
		
		<section>
			<h2>Demo</h2>
			<p>
				<a href="http://qranberry.me/roberto">Professional demo</a><br>
				Scan this QR code with your smartphone to access the demo.
			</p>
			<div style="overflow:auto;">
				<img id="qr_demo_image" src="<?php echo site_url ('assets/img/qr_professional.png');?>">
				<img id="qr_demo_code" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://qranberry.me/roberto&choe=UTF-8">
			</div>			
		</section>
		
		<section>
			<h2>Do's</h2>
			<ul>
				<li><b>Give a reason to scan your QR code.</b> Print your code on your card or CV with something like: 
				<ul>
					<li>"See my portfolio"</li>
					<li>"Past projects"</li>
				</ul>
				
				<li><b>Build a nice site.</b> Your landing page (the page people arrive at from scanning your QR code) could be either a homepage or a portfolio page. A homepage could include a navigational menu with links to your Portfolio, CV, Linkedin and Twitter profiles, plus contact info, and a brief bio. The portfolio page should include pictures and text that show your experience and skills.</li>

				<li><b>Get the job.</b> If you are appliying for a job, you can make a landing page specifically for this ocasion and add a video stating why you want to work there. It will make you memorable.</li>
			</ul>
		</section>
		
		<section>
			<h2>Don'ts</h2>
			<ul>
				<li><b>Don't be wordy.</b> Keep the text light, making your site easy to scan without long phrases or long paragraphs. Remember, QR viewers are viewing your content on a handheld phone.</li>
				<li><b>Don't overthink.</b> Don't try to have a perfect site in your first shot; just build it and have fun. If something doesn't work, you can change or add new ideas later.</li>
			</ul>
		</section>
	</div>
</div>


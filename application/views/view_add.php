<link rel="stylesheet" href="<?php echo site_url ('assets/js/chosen/chosen.css');?> " />

	
<script src="<?php echo site_url ('assets/js/keymaster.min.js');?>"></script>
<script src="<?php echo site_url ('assets/js/rangy_1_2_3/rangy-core.js');?>"></script>
<script src="<?php echo site_url ('assets/js/chosen/chosen.jquery.min.js');?>" type="text/javascript"></script>

<script src="https://raw.github.com/starfishmod/jquery-oembed-all/master/jquery.oembed.js"></script>


  
<script type="text/javascript">


$(document).ready(function()
{

	$().showtoggle();
	$().delete_photo();
	rangy.init();
	
	prepareEditor();
	initDroppable();


	var current_node = false;
	var current_subnode = false;

	//select menu -> chosen
	$(".chzn-select").chosen();
	
	//oembed
	$(".oembed").oembed(null,{
	    embedMethod: 'fill'    // "auto", "append", "fill" 
	} );  
		
	//prevent focus on "page""
	$("#html_page").click(function(data, handler){
		if (data.target == this) {
			(this).blur();
		}
	});

	$('#upload_target').load(function()
     {
		//get response in iframe
		var response = $('#upload_target').contents().find('#response').html();
		var json_obj = $.parseJSON( response );
		
		//hide ajax/loader
		$('#loader').hide();
		
		//show form
		$('#file_upload_form').show();
		
		//show photo
		if ( json_obj.status == 'ok' ){
			var photo_url = json_obj.path;			
			$('img', current_node).attr("src", photo_url);
		}
		$('#dialog-image').dialog( "close" );
	 });


	$('#file_upload_form').submit( function() 
	{	
		document.getElementById('file_upload_form').target = 'upload_target'; //'upload_target' is the name of the iframe
		
		//hide form
		$('#file_upload_form').hide();
		
		//show ajax/loader
		$('#loader').show();
	});
  
    $("#html_page").sortable({ handle: '.handle' });
	
	function initDroppable() {
		$(".draggable").draggable( {revert:true} );
		
		$(".handable_wrap").droppable(
		{
			greedy: true,
			drop: function(event, ui) 
			{
				var data_html = ui.draggable.attr('data-html');				
				$(data_html).insertAfter(this);
                $("p").droppable("destroy");
                initDroppable();
				makeHandable();
                event.preventDefault();
                return false;			
			}
		});		

		$(".droppable").droppable(
		{
			drop: function(event, ui) 
			{
				var data_html = ui.draggable.attr('data-html');				
				$(data_html).appendTo(this);
                $("p").droppable("destroy");
                initDroppable();
				makeHandable();
                event.preventDefault();
                return false;			
			}
		});
		
	};
	
	
	function makeHandable()
	{
		$('.handable').each(function(index) {
			$(this).wrap('<div class="handable_element" />');
			$(this).parent().wrap('<div class="handable_wrap ui-droppable" />');						
			$(this).parent().parent().prepend('<span contenteditable="false" class="handle"><?php echo lang('move');?></span><span contenteditable="false" class="edit"><?php echo lang('edit');?></span><span contenteditable="false" class="delete"><?php echo lang('delete');?></span>');	
			$(this).removeClass('handable ui-droppable');
		});
	}
		
	function prepareEditor ()
	{
		$('#html_page').children().each( function ()
		{
			$(this).addClass('handable');
		});
		makeHandable();
	}

	key ('backspace', function(e) { //prevent node delation for H#s & Ps
		var tag_name =   current_node[0].tagName;
		var h_p = ["P", "H1", "H2", "H3", "H4", "H5", "H6"];
		var length = current_node.text().length;
		if ( jQuery.inArray( tag_name, h_p) != -1  && length == 0){
			e.preventDefault();
		}
	});

	$('.handable_element p').live("click", function() {
		current_node = $(this);
	});
	
	$('.handable_element :header').live("click", function() {
		current_node = $(this);
	});
	
	key('enter', function(e){ 
		e.preventDefault();
		insertNodeAtRange();
	});
		
    function getFirstRange() {
        var sel = rangy.getSelection();
        return sel.rangeCount ? sel.getRangeAt(0) : null;
    }
	
    function insertNodeAtRange() {
        var range = getFirstRange();
        if (range) {
            var el = document.createElement("br");
            range.insertNode(el);
            rangy.getSelection().setSingleRange(range);
			range.setStartAfter(el);
			range.collapse(true);				
        }
    }

		

		// Enable multiple selections in IE
		try {
			document.execCommand("MultipleSelection", true, true);
		} catch (ex) {}

		
		
		//deletion
		$('#html_page .delete').live("click",function () {
			handable_wrap =  $(this).parent();
			handable_wrap.hide('medium', function () { handable_wrap.remove();} );
			$('#html_page').blur();
		});		
		
		//edition
		$('.edit').live("click", function() {
			var handable_wrap =  $(this).parent();
			current_node = $('div', handable_wrap).children().first();
			var tag_name =   current_node[0].tagName;
			
			switch (tag_name){
				case 'P':
				break;
				case 'H1':
				case 'H2':
				case 'H3':
				case 'H4':
				case 'H5':
				case 'H6':
					$( "#dialog-header" ).dialog({ width: 500, modal: true });
				break;
				default : 
					edit_class ();
			}
		});
		
		function edit_class ()
		{
			var node_class = current_node.attr('class').split(' ')[0];			
			switch (node_class){
				case 'button':
					edit_button ();
					break;
				case 'menu':
					edit_menu ();
					break;
				case 'image':
					edit_image();
					break;
				case 'embed':
					edit_embed();
					break;
			}			
		}
		
// HEADER ---------------------------------------------------------------/		
		// HEADER SETTINGS		
		$('.settings_header').click( function (e){ //convert h# to h#
			e.preventDefault();
			var value = $(this).attr('data-value');
			var new_node = $('<'+value+'/>').append(current_node.contents());
			current_node.replaceWith(new_node);			
			new_node.addClass('ui-droppable');
			current_node = new_node;
		});
		
// BUTTON & MENU --------------------------------------------------------/		
		// BUTTON SETTINGS
		function edit_button ()
		{
			//prepare dialog
			$('#dialog-button-content').empty();
			$( "#dialog-button" ).dialog({ width: 500, modal: true });	
			//make form
			current_node.attr('data-id', form_settings ( current_node, 'button' ) );
		}
		

		//ITEM FORM settings
		function form_settings ( element, button_or_menu )
		{
			//add data-id
			var random_str = Math.floor(Math.random()*10000000);
			var form = $('#button_form').clone().appendTo('#dialog-'+button_or_menu+'-content');
			form.show();			
			form.attr( 'data-id', random_str );
				
			//fill fields
			var button_node = $("a:not(span)", element);			
			var button_url = button_node.attr( 'href' );
			var button_text = button_node.text();				
			$('input[name="button_text"]', form).val( button_text );
			$('input[name="button_url"]', form).val( button_url );

			return random_str;
		}
		
		//BUTTON/MENU INPUTS
		$('.button_form input').live('keyup', function (e) {
	
			var attr_name = ( $(this).attr('name') );

			if ( attr_name == 'button_text' )
				current_subnode.text( $(this).val() );
			if ( attr_name == 'button_url' )
				current_subnode.attr( 'href', $(this).val() );
		});
		
		//BUTTON/MENU DELETE
		$('.button_delete').live('click', function (e) {
			e.preventDefault();
			
			var form_item = $(this).parent();
			var id = form_item.attr('data-id');
			var item_removed = $( 'li:[data-id="'+id+'"]');
			item_removed.hide('medium', function() { item_removed.remove(); });
			form_item.hide('medium', function() { form_item.remove(); });

		});

		//BUTTON/MENU SELECT PAGE
		$('#select_page').live('change', function (e) {
			var data_id = $(this).parent().attr('data-id');
			current_subnode = $('[data-id='+data_id+'] a' ).not('.button_delete');
			var new_url = $(this).val();
			var new_text = $('option:selected', this).text();	
			var form_item = $(this).parent();
			var id = form_item.attr('data-id');
			$('input[name="button_url"]', form_item).val( new_url );
			$('input[name="button_text"]', form_item).val( new_text );
			
			current_subnode.text( new_text);
			current_subnode.attr( 'href', new_url );	
		});


		//ITEM FORM FOCUS
		$('.button_form input').live('focus', function (e) {
			var data_id = $(this).parent().attr('data-id');
			current_subnode = $('[data-id='+data_id+'] a' ).not('.button_delete');
		});

		// MENU SETTINGS
		function edit_menu ()
		{
			//prepare dialog
			$('#dialog-menu-content').empty();
			$( "#dialog-menu" ).dialog({ width: 500, modal: true });	
			
			//make inputs for every button
			$('li', current_node).each( function ()
			{
				$(this).attr('data-id', form_settings ( this, 'menu' ) );	//make form
			});
		}
		
		//add new UL LI item
		$('#add_url_item').click ( function ()
		{
			current_node.append('<li><a href="http://qranberry.me">new</a></li>' );			
			edit_menu ( current_node );
		});
				
// IMAGE ------------------------------------------------------------/	

		//IMAGE SETTINGS
		//open dialog
		function edit_image () 
		{
			$( "#dialog-image" ).dialog({ width: 500, modal: true });
			var current_href= $('a img', current_node ).parent().attr('href');
			if ( current_href ) {
				$('#image_link').val( current_href );
			}
		};
			
		//select photo from gallery
		$('.editor_img img').click( function () {
			var src = $(this).attr('src');
			$('img', current_node).attr("src", src );
		});
		
		//image link image_link
		$('#image_link').live('keyup', function (e) {
			var link_url = ( $('#image_link').val() );
			var this_link = $('a img', current_node );
			
			//no href
			if ( link_url == '' )
			{
				//but it was a link, the unwrap
				if ( this_link.length > 0 ){
				 	$('img', current_node).unwrap();				
				}
			}
			else {
				//new link
				if ( this_link.length == 0 ){ 
					$('img', current_node).wrap('<a href="'+link_url+'">  <a/>');				
				}
				//old exist
				else {
					$( this_link ).parent().attr( 'href', link_url );
				}				
			}
		});

		//image select page
		$('.img_select_page').live('change', function (e) {
			var parent = $(this).parent();
			var link_url = $(this).val();
			var this_link = $( 'a img', current_node );
			$('#image_link').val( link_url );		
					
			//link still doesn't exist
			if ( this_link.length == 0 ){ 
				$('img', current_node).wrap('<a href="'+link_url+'">  <a/>');				
			}
			//link exist
			else {
				$( this_link).parent().attr( 'href', link_url );
			}
		});	


// EMBED ------------------------------------------------------------/	

		function edit_embed () 
		{
			$('#embed_input').val("");
			$( "#dialog-embed" ).dialog({ width: 500, modal: true });
		};
		
		//update embed
		$('#embed_submit').live("click", function (){
			var value = $('#embed_input').val();
			var current_oembed = $(".oembed", current_node).attr( 'href', value );

			current_oembed.text(value);
			current_oembed.oembed(null,{embedMethod: 'fill'} );  

			$('#dialog-embed').dialog( "close" );			
		});
		
		
// UPDATE ----------------------------------------------------------/		
		//Prepare textarea with the wysiwyg content
		$('#update').click( function () {
						
			$('.oembed').each ( function ()
			{
				$(this).text("");	
			});		
			
			var cloned = $('#html_page').clone();			
			$('span', cloned).remove();			
			$('.handable_element', cloned).unwrap();
			$('.handable_element', cloned).children().unwrap();
			
			cloned.children().removeClass('ui-droppable');			
			$('#html_textarea').val( cloned.html() );
						
			document.forms["add_form"].submit();
		});
		
		
		//THEME SWITCHER
		$('#theme_switcher').change( function () 
		{
			var value = $(this).val();
			var css_url = "<?php echo site_url ('assets/css/themes/REPLACE_CSS/style.css');?>";
			var new_css = css_url.replace("REPLACE_CSS",  value );			
			$("#current_theme").attr({href : new_css });
			$('input[name="user_theme"]').val( value );
		});

});
</script>

<div class="content">
	
	<div id="editor_instructions"><?php echo lang('editor instruction');?></div>
	<div contenteditable="true" id="html_page" class="page droppable"><?php if ( $page_db ) echo $page_db->page_description;?></div>
	
	<form id="add_form" method="post" action="<?php echo site_url ('site/add');?>">
		<textarea name="page_description" class="content_hidden" id="html_textarea"></textarea>
		<br>
		<input class="styled_form long_text" type="text" name="page_title" value="<?php if ( $page_db ) echo $page_db->page_title;?>" placeholder="<?php echo lang('placeholder pagename');?>">
		<input type="hidden" name="user_theme" value="<?php echo $user_theme;?>">
		<a class="button_cta button_big button_green" href="#" id="update"><?php echo lang('save & publish');?></a>
	</form>
</div>

<!-- Editor -->
<div id="editor">
	<?php echo lang('select theme');?><br>
	<!-- Themes-->
	<select id="theme_switcher" style="width:100%;" class="chzn-select">	
	<?php
	if ( $themes )
	foreach ( $themes as $theme )
	{?>
		<option name="theme" value="<?php echo $theme;?>" 
		<?php  if ( $theme == $user->user_theme ) echo ' selected="selected" ';?>> 		
		<?php echo ucfirst ( $theme ); 
			if ( in_array ( $theme, $this->config->item( 'pro_themes') ) ) echo " (Pro)";
			if ( in_array ( $theme, $this->config->item( 'enterprise_themes') ) ) echo " (Enterprise)";
		?></option>
	<?php
	}?>
	</select>
		
	<div class="draggable" data-html="<h1 class='handable'><?php echo lang('click here to edit');?></h1>"><?php echo lang('header');?></div>
	<div class="draggable" data-html="<p class='handable'><?php echo lang('click here to edit');?></p>"><?php echo lang('text');?></div>	
	<div class="draggable" data-html="<div class='handable button'><a href='http://google.com'>Button text</a></div>"><?php echo lang('button');?></div>
	<div class="draggable" data-html="<ul class='handable menu'><li><a href='http://google.com'>Item 1</a></li><li><a href='http://google.com'>Item 2</a></li></ul>"><?php echo lang('menu');?></div>
	<div class="draggable" data-html="<div class='handable image'><img  src='http://placehold.it/480x300&text=IMAGE'></div>"><?php echo lang('image');?></div>	  
	<div class="draggable" data-html="<div class='handable embed'><a class='oembed' href='#'><img  src='http://placehold.it/463x150&text=EMBED'></a></div>"><?php echo lang('embed');?></div>	  

</div>



<!-- DIALOGS -->

<!-- Embed -->
<div id="dialog-embed" class="dialog" title="<?php echo lang('embed');?>">
	<?php echo lang('enter a url');?>
	<input class="styled_form long_text" id="embed_input" type="text">
	<a href="#" id="embed_submit" class="button_cta button_green"><?php echo lang('embed');?></a>
	<br><br>
	<?php echo lang('examples');?>:<br>
	<span class="light_text">
	http://www.youtube.com/watch?v=oHg5SJYRHA0<br>
	http://vimeo.com/40977797<br>
	http://soundcloud.com/alfred-packer/freelance-whales-hannah-bass<br>
	http://dribbble.com/shots/545412-Kawaii-Han-and-Chewie
	</span>	
</div>

<!-- Header -->
<div id="dialog-header" class="dialog" title="Header">
	<?php echo lang('select header size');?>:<br>
	<a href="#" class="settings_header" data-action="header_type" data-value="h1">H1</a>
	<a href="#" class="settings_header" data-action="header_type" data-value="h2">H2</a>
	<a href="#" class="settings_header" data-action="header_type" data-value="h3">H3</a>
	<a href="#" class="settings_header" data-action="header_type" data-value="h4">H4</a>
</div>

<!-- Menu -->
<div id="dialog-menu" class="dialog" title="<?php echo lang('menu');?>">
	<a href="#" id="add_url_item"><?php echo lang('add item');?></a>
	<div id="dialog-menu-content"></div>
</div>

<!-- Button -->
<div id="dialog-button" class="dialog" title="<?php echo lang('button');?>">
	<div id="dialog-button-content"></div>
</div>

<!-- Item form -->
<div id="button_form" class="button_form content_hidden">
	<?php echo lang('text');?>:<br>
	<input class="styled_form long_text" type="text" name="button_text" value=""><br>
	<?php echo lang('url');?>:<br>
	<input class="styled_form long_text" type="text" name="button_url" value=""><br>

	<select style="width:100%;" id="select_page" >	
		<option><?php echo lang('link to your pages');?></option>
	<?php
	if ( $pages )
	foreach ( $pages as $user_page )
	{?>
		<option name="page_id" value="<?php echo site_url ( 'id/'.$user_page->page_id );?>" ><?php echo $user_page->page_title;?></option>
	<?php
	}?>
	</select>
	<br><br>
	<a class="button_cta button_delete button_red"><?php echo lang('delete');?></a>
</div>

<!-- Image -->
<div id="dialog-image" class="dialog" title="<?php echo lang('image');?>">
	<label><?php echo lang('upload an image');?></label>
	
	<!-- img upload -->
	<form id="file_upload_form" method="post" enctype="multipart/form-data" action="<?php echo site_url('site/upload');?>">
		<input id="file_input" name="userfile" 	type="file" />
		<input type="submit" name="action" value="Save" /><br />
		<iframe id="upload_target" name="upload_target" src="" style="width:0;height:0;border:0px solid #fff;"></iframe>
	</form>	
	
	<div id="loader" style="display:none;"><img src="<?php echo site_url('assets/img/loader.gif');?>" alt=""></div>
	
	<!-- img library -->
	<?php if ($images)
	{
		echo lang('or pick an image'); ?>
		<div id="show_photos">
		<?php
		foreach ( $images as $image )
		{?>
			<div class="editor_img">
				<img src="<?php echo site_url ( $this->config->item ('upload_user_path' ).$user->user_id.'/'.$image ); ?>">
			</div>
		<?php
		}?>
		</div>
	<?php	
	}
	?>
	
	<!-- img link -->
	<br>
	Make this image into a link:<br>
	<input class="styled_form long_text" type="text" id="image_link" placeholder="http://">
	
	<select style="width:100%;" class="img_select_page" >	
		<option><?php echo lang('link to your pages');?></option>
	<?php
	if ( $pages )
	foreach ( $pages as $user_page )
	{?>
		<option name="page_id" value="<?php echo site_url ( 'id/'.$user_page->page_id );?>" ><?php echo $user_page->page_title;?></option>
	<?php
	}?>
	</select>
			
</div>


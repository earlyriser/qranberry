<body>
	<script type="text/javascript">
	$(document).ready(function()
	{				

		
		//SHOW/TOGGLE CONTENTS
		$(".content_hidden").css("display","none");	//hide on load
		
		$(".slideshow .left a").click(function(){		//on click
			var id = $(this).attr('data-slide');		

			//hide
			var old_selected = $('.slideshow .left .selected');
			var old_id = old_selected.attr('data-slide');
			old_selected.removeClass('selected');
			$('#'+old_id).hide('fast');

			//show
			$("#"+id).show('fast'); 
			$(this).addClass('selected');
			
			return false;
		});		
	});	
	
	</script>
	<script type="text/javascript" src="http://cdn.sublimevideo.net/js/xudvcwjz.js"></script>
	
	<div id="header" class="header_home">
		<div class="home content">
			<div id="home_i18n">
				<a href="<?php echo site_url ();?>">English</a> |
				<a href="<?php echo site_url ('es');?>">Español</a> |
				<a href="<?php echo site_url ('fr');?>">Français</a>
			</div>	
			
			<div id="login">
			<?php if ($this->session->userdata('logged_in') )
			{?>
				<a href="<?php echo site_url ('account'); ?>"><?php echo lang ('account');?></a>
				<a href="<?php echo site_url ('general/logout'); ?>"><?php echo lang ('logout');?></a>
			<?php
			}
			else
			{?>
				<a href="<?php echo site_url ('general/signup');?>"><?php echo lang ('signup');?></a>
				<a href="<?php echo site_url ('general/login');?>"><?php echo lang ('login');?></a>
			<?php
			} ?>
			</div>
		</div>
	</div>	
	
	<div class="home">
		<header>
			<h1><?php echo lang ('promote your');?> <img alt="Qranberry" style="width:200px;vertical-align:middle;" src="<?php echo site_url('assets/img/big_logo3.png');?>"></h1>
			<h2><?php echo lang ('in minutes');?></h2>
		</header>

		
		<div class="slideshow">
			<div class="left">
				<ul>
					<li><a class="selected" data-slide="real_state" href="#">Real Estate</a></li>
					<li><a data-slide="restaurant" href="#">Restaurants</a></li>
					<li><a data-slide="products" href="#">Products</a></li>
				</ul>
			</div>
			
			<div class="right">
				<div id="real_state">
					<img alt="" src="<?php echo site_url ('assets/img/home-house.png');?>">
					<p><?php echo lang('slideshow_realstate');?></p>
				</div>
				<div class="content_hidden" id="restaurant">
					<img alt="" src="<?php echo site_url ('assets/img/home-restaurant.png');?>">
					<p><?php echo lang('slideshow_restaurant');?></p>
				</div>					
				<div class="content_hidden" id="products">
					<img alt="" src="<?php echo site_url ('assets/img/home-cactus.png');?>">
					<p><?php echo lang('slideshow_product');?></p>
				</div>				
			</div>
		</div>

		<div style="text-align: center;"><a class="button button_green home_button_big" href="http://qranberry.me/general/signup">Create your site (free)</a></div>

		<?php if ( lang('current_lang') =='en' OR lang('current_lang') =='fr')
		{?>
		<section class="group">
			<blockquote><?php echo lang ('home ribbon');?></blockquote>
			<div id="video_container">
				<video id="home_video" class="sublime" width="640" height="360" poster="<?php echo site_url ('assets/video/qranberry_'.lang('current_lang').'.jpg');?>" preload="none">
				  <source src="<?php echo site_url ('assets/video/qranberry_'.lang('current_lang').'.mp4');?>" />
				</video>
			</div>
		</section>
		<?php  
		} ?>		
		
		<section>
			<h1><?php echo lang ('plans and pricing');?></h1>
			<table id="table_plans">
				<thead>
				<tr id="plan_types">
					<th></th>
					<th>Free<span class="plan_prices">$0</span></th>
					<th>Pro<span class="plan_prices">$9</span><?php echo lang('monthly');?></th>
					<th>Enterprise<span class="plan_prices">$24</span><?php echo lang('monthly');?></th>
				</tr>
				</thead>
				<tbody>
			
				<tr>
					<td> <?php echo lang ('pages');?></td>
					<td>5</td>
					<td>50</td>
					<td>1000</td>
				</tr>
				<tr>
					<td> <?php echo lang ('themes');?></td>
					<td>1</td>
					<td>3</td>
					<td>All</td>
				</tr>
				<tr>
					<td> <?php echo lang ('custom urls');?></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/x_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>
				<tr>
					<td> <?php echo lang ('ads free');?></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/x_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>
				<tr>
					<td> <?php echo lang ('stats');?></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img alt="" src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>								
				</tbody>
			</table>
		</section>	
		
		<!-- FOOTER -->
		<div id="footer">
			<div class="content">
				<div id="footer_links">
					<a href="<?php echo site_url ("section/tos");?>"><?php echo lang ('TOS');?></a> | <a href="<?php echo site_url ("section/privacy");?>"><?php echo lang ('PP');?></a> | <a href="<?php echo site_url ("section/contact");?>"><?php echo lang("contact");?></a> | <a href="#" class="content_switch" data-toggle="0" data-interact-id="credits"><?php echo lang ('photo credits');?></a>

					<div id="credits" class="content_hidden">
						<a href="http://www.flickr.com/photos/jeffreyww/4602657102/">Mmm... lasagna</a>, <a href="http://www.flickr.com/people/jeffreyww/">jeffreyww</a><br>
						<a href="http://www.flickr.com/photos/pnwra/433449696">House in Vancouver</a>, <a href="http://www.flickr.com/photos/pnwra">pnwra</a>
					</div>
				</div>

				<div id="footer_social">
					<!-- FB-->
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fqranberry.me&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" style="border:none; overflow:hidden; width:90px; height:21px;"></iframe>
				
					<!-- Twitter-->
					<a href="https://twitter.com/share" class="twitter-share-button" data-text="Promote your business with Qranberry" data-count="none">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>				
			</div>
			

		</div>
		
	</div>
		
	<!-- GOOGLE PLUS Place this tag in your head or just before your close body tag -->
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	
</body>
</html>
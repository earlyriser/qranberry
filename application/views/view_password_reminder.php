<div class="page">
	<h1><?php echo lang ('get your password');?></h1>
	
	<div class="page_content">
		<form method="post" action="<?php echo site_url ('general/password_reminder');?>">
			<!-- EMAIL -->
			<label><?php echo lang ('email');?></label>
			<input class="styled_form long_text" name="email" 	type="text" value="" />

			<input class="button button_big button_green" type="submit" value="<?php echo lang ('submit');?>">
		</form>
	</div>
	
</div>
<style>
fieldset
{
	background-color: #eee;
}

label 
{	
	display:	block;
	font-size:	1em;
	margin: 	0 0 0.1em 0;
font-weight:bold;
}

input
{
	border:			2px solid #ddd;
	border-radius: 	7px;
	color: 			#444;
	font-family:	Georgia, serif;
	font-size:		1em;
	padding:		5px;
margin-bottom:1em;
}

</style>

<script type="text/javascript" src="<?php echo site_url ('assets/js/jscolor/jscolor.js')?>"></script>

<script>
    $(document).ready(function(){
		//COLORS
		$('.color').change( function ()
		{
			var element = $(this).attr('name');
			var value = $(this).val();
			
			switch (element)
			{
				case "bg":					$('body').css('background', '#'+value);					break;

				case "page-color":			$('.page').css('color', '#'+value);								break;
				
				case "header-color":		$(':header').css('color', '#'+value);								break;
				
				case "link-color":			$('.page a').not('.button, .menu').css('color', '#'+value);		break;
				
				case "image-bg":			$('.page .image').css('background-color', '#'+value);	console.log ( value ); break;
				
				case "button-bg":			$('.page .button, .page .menu').css('background', '#'+value);			break;	
				case "button-color":		$('.page. button, .page.menu').css('color', '#'+value);					break;
				case "buttonhover-bg":		$('.page .button:hover, .page .menu:hover').css('background', '#'+value);	break;
				case "buttonhover-color":	$('.page .button:hover, .page .menu:hover').css('color', '#'+value);		break;
			}

		});

		//FONT
		$('#font').change( function ()
		{
			var value = $(this).val();
			console.log ('change');
			$('.page').css('font-family', value);
		});
		
	   //HOVERS
	   $('.button, .menu').hover( function(){
		  $(this).css('background', "#"+$('input[name="buttonhover-bg"]').val() );
		  $(this).css('color', "#"+$('input[name="buttonhover-color"]').val() );
	   },
	   function(){
		  $(this).css('background', "#"+$('input[name="button-bg"]').val() );
		  $(this).css('color', "#"+$('input[name="button-color"]').val() );
	   });
    });
</script>

<div class="content page">
<form method="post">
	<h1>Style editor</h1>
	Use this editor to change your style settings.<br>
	
	<!--General-->
	<label>Background color</label>	
	<input class="color" name="bg"><br>
	<label>Text color</label>		
	<input class="color" name="page-color"><br>
	<label>Font</label>
		<select name="font" id="font">
			<option value="Arial">Arial</option>
			<option value="Courier New">Courier New</option>
			<option value="Georgia">Georgia</option>
			<option value="Tahoma">Tahoma</option>
			<option value="Times New Roman">Times New Roman</option>
			<option value="Verdana">Verdana</option>
		</select>
	<br>

	<!--Headers-->
	 <h1>Heading 1</h1>
	 <h2>Heading 2</h2>
	 <h3>Heading 3</h3>
	 <h4>Heading 4</h4>
	 <h5>Heading 5</h5>
	 <h6>Heading 6</h6>
	<label>Headers color</label>
	<input class="color" name="header-color"><br>
	
	<!--Links-->
	<a href="#">This is a test link</a><br>
	This is a sentence with a <a href="#">link</a> and some words.<br>
	<label>Links color</label>
	<input class="color" name="link-color"><br>
	
	<!--Images-->
	<img class="image" src="http://placehold.it/300x200">
	<label>Image border color</label>
	<input class="color" name="image-bg"><br>
	
	<!--Buttons-->
	<a href="#" class="button">Test button</a>
	<label>Buttons text color</label>
	<input class="color" name="button-color"><br>
	<label>Button background</label>
	<input class="color" name="button-bg"><br>
	<label>Hovered button | text color</label>
	<input class="color" name="buttonhover-color"><br>
	<label>Hovered button | Background</label>
	<input class="color" name="buttonhover-bg"><br>

</form>




 

	



		
</div>
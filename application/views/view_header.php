<body>
<?php
//page
if ( $view == 'view_page')
{?>

	<div class="content">
		<div class="top_page">
			<?php if ( $page->user_plan != "gratis")
			{?>
			<a href="<?php echo site_url('account');?>"><?php echo lang ('account');?></a>
				<a href="<?php echo site_url ($page->user_name);?>"><img src="<?php echo site_url ('assets/img/icons/home_16x16.png');?>"></a>
		</div>
		<?php
		} ?>
	</div>
<?php
}

//OTHER PAGES
else
{?>
<div id="header">
	<div class="content">
		<a id="logo" href="<?php echo site_url ( $this->session->userdata('lang') );?>">
			<img src="<?php echo site_url ('assets/img/big_logo3.png');?>" alt="" height="30">
		</a>	
		<div id="user_menu" >
		<?php if ($this->session->userdata('logged_in'))
		{ 
			/* gravatar 
			<img src="http://www.gravatar.com/avatar/<?php echo md5( strtolower( trim( $user->user_email ) ) ) ;?>?s=25"> */
			?>
			<a href="<?php echo site_url ('account'); ?>"><?php echo lang ('account');?></a>
			<a href="<?php echo site_url ('general/logout'); ?>"><?php echo lang ('logout');?></a>
		<?php
		} 
		else
		{?>
			<a href="<?php echo site_url ('general/login'); ?>"><?php echo lang ('login');?></a>		
		<?php
		}
		?>
		</div>
	</div>
</div>
<?php
}
 
//ALERT
if ( $this->session->flashdata('alert') )
{?>
<div class="content">
	<div class="alert">
		<?php
			if ( $this->session->flashdata('alert') == 'msg_page_added' )
			{
				$alert = lang ( 'msg_page_added' );
				$alert = str_replace ( array( '*URL_QR*', '*URL_ADD*' ), array( site_url( 'account/printing/'.$page->page_id ), site_url('site/add') ), $alert );
				echo $alert;
			}
			else
			{
				echo lang ( $this->session->flashdata('alert') );
			}?>
	</div>
</div>
<?php
}
?>
<div class="page">
	<h1><?php echo lang ('plans');?></h1>
	
	<div class="page_content">
	
		<h2><?php echo lang ('your current plan').' '.$user->user_plan;?></h2>		
				
		<?php
		if ( $user->user_plan == 'pro' or $user->user_plan == 'enterprise' )
		{?>
			<form method="post" action="<?php echo site_url('account/plans');?>">
				<input type="hidden" name="interrupt" value=1>
				<button class="button button_red"><?php echo lang('interrupt plan');?></button>
			</form>
		<?php
		} ?>
		
		<br>
		<?php if ( $user->user_plan == 'gratis' )
		{?>			
			<table id="table_plans">
				<thead>
				<tr id="plan_types">
					<th></th>
					<th>Free<span class="plan_prices">$0</span></th>
					<th>Pro<span class="plan_prices">$9</span><?php echo lang('monthly');?></th>
					<th>Enterprise<span class="plan_prices">$24</span><?php echo lang('monthly');?></th>
				</tr>
				</thead>
				<tbody>
			
				<tr>
					<td> <?php echo lang ('pages');?></td>
					<td>5</td>
					<td>10</td>
					<td>50</td>
				</tr>
				<tr>
					<td> <?php echo lang ('themes');?></td>
					<td>1</td>
					<td>3</td>
					<td>All</td>
				</tr>
				<tr>
					<td> <?php echo lang ('custom urls');?></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/x_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>
				<tr>
					<td> <?php echo lang ('ads free');?></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/x_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>
				<tr>
					<td> <?php echo lang ('stats');?></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
					<td><img src="<?php echo site_url ('assets/img/icons/iconic/gray_dark/check_alt_24x24.png');?>"></td>
				</tr>				
				
				<tr>
					<td></td>
					<td></td>
					<td>
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="W74DJ9JLR24HG">
						<input type="hidden" name="custom" 			value="<?php echo $user_id?>" />
						<input type="hidden" name="notify_url" value="<?php echo site_url('paypal/ipn');?>">	
						<button type="submit" class="button button_green"><?php echo lang('signup_plan');?></button>	
						<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
						</form>
					</td>
					<td>
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="42TFT6B93L5GA">
						<input type="hidden" name="custom" 			value="<?php echo $user_id?>" />
						<input type="hidden" name="notify_url" value="<?php echo site_url('paypal/ipn');?>">	
						<button type="submit" class="button button_green"><?php echo lang('signup_plan');?></button>						<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
						</form>
					</td>
				</tr>
				</tbody>				
			</table>
			<br>		
		<?php
		}
		?>
	</div>
</div>

<?php 

/*
						<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
							<input type="hidden" name="cmd" 			value="_xclick-subscriptions">
							<input type="hidden" name="business" 		value="contact@qranberry.me">
							<input type="hidden" name="item_name"	 	value="pro_monthly">
							<input type="hidden" name="currency_code" 	value="CAD">
							<input type="hidden" name="a3" 				value="9.00">
							<input type="hidden" name="p3" 				value="1">
							<input type="hidden" name="t3" 				value="M">
					
							<input type="hidden" name="src" 			value="1">
							<input type="hidden" name="no_note" 		value="1">
							<input type="hidden" name="custom" 			value="<?php echo $user_id?>" />

							<input type="hidden" name="notify_url" value="<?php echo site_url('paypal/ipn');?>">	
							<button type="submit" class="button button_green"><?php echo lang('signup');?></button>				
						</form>
						*/ ?>
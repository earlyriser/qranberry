

<script>
$(document).ready(function()
{

// Check the textbox every 100 milliseconds.  This seems to be pretty responsive.
setInterval(watchTextbox, 100);

// Compare the textbox's current and last value.  Report a change to the console.
function watchTextbox() {
  var txtInput = $('#qr_title');
  var lastValue = txtInput.data('lastValue');
  var currentValue = txtInput.val();
  if (lastValue != currentValue) {
		$('#print_title').empty();
		$('#print_title').append( currentValue);
  }
}
	
});
</script>

	<!-- PRINT -->
	<div id="qr_tools">
		<input id="qr_title" class="styled_form" value="<?php echo lang ('your title');?>">
		<a href="#" id="print_link" class="button button_small button_green" onclick='window.print();'><?php echo lang ('print qr code');?></a>
	</div>

	<div id="print_page">
		<div id="print_title"></div>
		<a href="<?php echo site_url ( 'id/'.$page->page_id );?>"><img class="qr_code" src="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=<?php echo site_url ('id/'.$page->page_id);?>&choe=UTF-8"></a>
	</div>

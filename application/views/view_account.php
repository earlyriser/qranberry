<div class="page">
	<h1><?php echo lang ('pages');?></h1>
	
	<div class="page_content">
	
	<h2><?php echo lang ('create page');?></h2>
	<a class="button button_green" href="<?php echo site_url ('site/create');?>"><?php echo lang ('create page');?></a>
	<br><br>
	
	<?php
	if ( $pages )
	{
		
		//PRO Homepage
		if ( $user->user_plan == 'pro' )
		{
			?>
			<h2><?php echo lang ('homepage');?></h2>
			<form action="<?php echo site_url ('account/set_home');?>" method="post">
				<select name="homepage" ">
				<?php
				foreach ( $pages as $page )
				{?>
					<option value="<?php echo $page->page_id;?>" <?php if ( $page->page_id == $user->user_home ) echo "selected='selected'";?>>
						<?php echo $page->page_title;?>
					</option>
				<?php
				}?>
				</select>
				<button type="submit" class="button button_gray"><?php echo lang('change homepage');?></button>
			</form>
			<?php echo lang('the homepage is what people will see at');?> <a href="<?php echo site_url($user->user_name);?>">qranberry.me/<?php echo $user->user_name;?></a><br><br>			
			<?php
		}?>
		
		<h2><?php echo lang ('pages');?></h2>
		<table class="account_pages">
		<?php
		//pageS LIST
		foreach ( $pages as $page )
		{?>
			<tr>
				<td>
					<!--Name-->
					<a class="page_name" href="<?php echo site_url ( 'id/'.$page->page_id );?>"><?php echo $page->page_title;?></a>
				
					<!-- Delete page-->
					<div class="content_hidden delete_page" id="delete_<?php echo $page->page_id;?>" data-toggle="0";  >
						<?php echo lang('do you really want to delete this page?');?><br>
						<form action="<?php echo site_url ('account/delete');?>" method="post">
							<input type="hidden" name="id" value="<?php echo $page->page_id;?>">
							
							<a href="#" class="button button_gray content_switch" data-interact-id="delete_<?php echo $page->page_id;?>" ><?php echo lang('cancel');?></a>
							<button type="submit" class="button button_red"><?php echo lang('delete');?></button>
						</form>
					</div>				
				</td>
				
				<td>
					<!-- Buttons -->
					<a href="<?php echo site_url ( 'account/edit/'.$page->page_id );?>" class="button button_gray">
						<img height=16 src="<?php echo site_url ('assets/img/icons/icon_pencil.png');?>">
					</a>
					<a href="<?php echo site_url ( 'account/printing/'.$page->page_id );?>" class="button button_gray">
						<img height=16 src="<?php echo site_url ('assets/img/icons/icon_qr.png');?>">
					</a>					
					<a href="#" class="button button_gray content_switch" data-interact-id="delete_<?php echo $page->page_id;?>">
						<img height=16 src="<?php echo site_url ('assets/img/icons/icon_delete.png');?>">
					</a>
				</td>
			</tr>
		<?php
		}?>
		</table>
	<?php
	}	
	?>
	</div>

</div>
	<div id="footer">
		<div class="content">
		<?php if ( $view == 'view_page')
		{?>
			<?php echo lang ('footer phrase');?> <a href="<?php echo site_url ();?>">Qranberry</a>
		<?php
		}
		else
		{?>
			<a href="<?php echo site_url ('section/tos');?>"><?php echo lang ('TOS');?></a> | 
			<a href="<?php echo site_url ('section/privacy');?>"><?php echo lang ('PP');?></a> | 
			<a href="<?php echo site_url ('section/contact');?>"><?php echo lang ('contact');?></a>
		<?php
		}
		?>
		</div>
	</div>
	
	<!--Google Stats-->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-25159432-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	

</body>
</html>
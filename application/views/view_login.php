<div class="page">
	<h1><?php echo lang ('login');?></h1>
	
	<div class="page_content">
		<form method="post" action="<?php echo site_url ('general/login');?>">
			<!-- EMAIL -->
			<label><?php echo lang ('email');?></label>
			<input class="styled_form long_text" name="email" 	type="text" value="<?php echo $email;?>" />

			<!-- PASSWORD -->
			<label><?php echo lang ('password');?></label>
			<input class="styled_form long_text" name="password"type="password" value="" />
			<a href="<?php echo site_url ('general/password_reminder');?>"><?php echo lang ('forgot it');?></a><br>
			
			<input class="button button_big button_green" type="submit" value="<?php echo lang ('submit');?>">
		</form>
	</div>
	
</div>
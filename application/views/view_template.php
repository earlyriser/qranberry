<?php
$this->load->view('view_meta');
$this->load->view('view_header');

if ( strpos  ( $view, 'account' ) !== false )
	$this->load->view('view_account_menu');

$this->load->view($view);
$this->load->view('view_footer'); 

?>
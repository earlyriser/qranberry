<div class="page">
	<h1><?php echo lang ('profile');?></h1>
	
	<div class="page_content">
		<form method="post" action="<?php echo site_url ('account/profile');?>">
		<?php 
		
		if ( $user->user_plan == 'pro' AND $user->user_name == '' )		
		{?>
			<!-- USERNAME -->
			<label><?php echo lang ('username');?></label>
			<?php echo lang('choose wisely') ;?>
			<input class="styled_form long_text" name="user_name" type="text" value="" />
			<?php echo lang('your site will be at') ;?> qranberry.me/<?php echo lang('my_username') ;?>
		<?php
		}?>	
			<!-- PASSWORD -->
			<label><?php echo lang ('password');?></label>
			<input class="styled_form long_text" name="password" type="password" value="" />
			
			<input class="button button_big button_green" type="submit" value="<?php echo lang ('submit');?>">
		</form>
	</div>
	
</div>
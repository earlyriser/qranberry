<div class="page">
	<h1><?php echo lang ('signup');?></h1>
	
	<div class="page_content">
		<form method="post" action="<?php echo site_url ('general/signup');?>">
			<!-- EMAIL -->
			<label><?php echo lang ('email');?></label>
			<input class="styled_form long_text" name="email" 	type="text" value="<?php echo $email;?>" />
			<?php echo form_error('email'); ?>
			
			<!-- PASSWORD -->
			<label><?php echo lang ('password');?></label>
			<input class="styled_form long_text" name="password"type="password" value="" />
			<?php echo form_error('password'); ?>
			
			<input class="button button_big button_green" type="submit" value="<?php echo lang ('signup');?>">
		</form>
	</div>
	
</div>
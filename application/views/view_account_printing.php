<div class="page">
	<h1><?php echo lang('printing');?></h1>
	
	<div class="page_content">	
		1. <?php echo lang('print step 1');?><br>
		2. <?php echo lang('print step 2');?><br>
		3. <?php echo lang('print step 3');?><br><br>

		<h2><?php echo lang('ml resto');?></h2>
		<div class="print_thumbnails">
		<a class="print_thumbnail_2" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/resto_tent-en');?>"><img width="215" src="<?php echo site_url ('assets/img/resto_tent-en.jpg');?>"></a>
		<a class="print_thumbnail_2" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/resto_tent-es');?>"><img width="215" src="<?php echo site_url ('assets/img/resto_tent-es.jpg');?>"></a>
		<a class="print_thumbnail_2" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/resto_tent-fr');?>"><img width="215" src="<?php echo site_url ('assets/img/resto_tent-fr.jpg');?>"></a>
		<a class="print_thumbnail_2" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/resto_tent-en_fr');?>"><img width="215" src="<?php echo site_url ('assets/img/resto_tent-en_fr.jpg');?>"></a>
		</div>
	
		<h2><?php echo lang('ml sale');?></h2>
		<div class="print_thumbnails">
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/sale-en');?>"><img src="<?php echo site_url ('assets/img/sale-en.jpg');?>"></a>
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/sale-es');?>"><img src="<?php echo site_url ('assets/img/sale-es.jpg');?>"></a>
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/sale-fr');?>"><img src="<?php echo site_url ('assets/img/sale-fr.jpg');?>"></a>
		</div>
		
		<h2><?php echo lang('ml rent');?></h2>
		<div class="print_thumbnails">
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/rent-en');?>"><img src="<?php echo site_url ('assets/img/rent-en.jpg');?>"></a>
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/rent-es');?>"><img src="<?php echo site_url ('assets/img/rent-es.jpg');?>"></a>
		<a class="print_thumbnail_3" href="<?php echo site_url ('account/mobile_link/'.$page_id.'/rent-fr');?>"><img src="<?php echo site_url ('assets/img/rent-fr.jpg');?>"></a>
		</div>	

		<h2><?php echo lang('ml perso');?></h2>
		<div class="print_thumbnails">
		<a class="print_thumbnail_2" href="<?php echo site_url ('code/'.$page_id);?>"><img src="http://placehold.it/100x140"></a>
		</div>

		<h2><?php echo lang('qr standalone');?></h2>
		<?php echo lang('download and integrate');?>
		<div class="print_thumbnails">
		<a class="print_thumbnail_2" href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=<?php echo site_url ('id/'.$page_id);?>&choe=UTF-8"><img src="<?php echo site_url ('assets/img/qr_100.jpg');?>"></a>
		</div>			
	</div>
	
</div>
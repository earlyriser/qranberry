

<script type="text/javascript" src="<?php echo site_url ('assets/js/jquery.sparkline.min.js');?>"></script>
<script type="text/javascript" src="<?php echo site_url ('assets/js/underscore-min.js');?>"></script>


<script>
$(document).ready(function()
{
	var stats = <?php echo $stats;?>;
	var days = [];
	var sparks = [];
	var today = new Date();
	var hr_offset = today.getTimezoneOffset()/60;
	today.setUTCHours ( hr_offset ); 
	var dateArray = getDates( today.addDays(-<?php echo $range;?>), today ); //make dates array

	//add 0 as counter value for each date
	_.each ( dateArray, function(value, key, list) {
		days [ value.toYMD() ] = 0; 
	});

	//increase counter for each visit on day
	_.each ( stats, function(stat) { 
		days[ stat.stat_timestamp ]++;
	});
		
	//make sparks array
	for(var value in days) {
	  sparks.push ( days[value]);
	}

	//show sparkine
	$('.dynamicsparkline').sparkline ( sparks, { type: 'bar',  barColor: '#cc0000', height:80, barWidth: 13, tooltipFormat: '{{value}}' }); });


    //date to yyyy-mm-dd
    Date.prototype.toYMD = function () {
        var year, month, day;
        year = String(this.getFullYear());
        month = String(this.getMonth() + 1);
        if (month.length == 1) {
            month = "0" + month;
        }
        day = String(this.getDate());
        if (day.length == 1) {
            day = "0" + day;
        }
        return year + "-" + month + "-" + day;
    }

	//add days to some date
	Date.prototype.addDays = function( numDays ) {
		var dat = new Date(this.valueOf())
		dat.setDate(dat.getDate() + numDays );
		return dat;
	}

	//get dates array from dateA-dateB
	function getDates(startDate, stopDate) {
		var dateArray = new Array();
		var currentDate = startDate;
		while (currentDate <= stopDate) {
			dateArray.push(currentDate)
			currentDate = currentDate.addDays(1);
		}
		return dateArray;
	}	
</script>

<div class="page">
	<h1><?php echo lang ('stats');?></h1>
	
	<div class="page_content">

		<h2><?php echo lang ('pageviews');?> | <?php echo lang ('last 30 days');?></h2>		
				
		<?php
		if ( $stats ){?>
			<span class="dynamicsparkline">...</span>
		<?php	
		} ?>

	</div>
</div>

<div class="page">
	<h1><?php echo lang ('style');?></h1>
	
	<div class="page_content">
	
		<form method="post" action="<?php echo site_url ('account/themes');?>">
		
			<!-- Themes-->
			<?php
			if ( $themes )
			foreach ( $themes as $theme )
			{?>
				<input class="radio_switch" type="radio" name="theme" value="<?php echo $theme;?>" <?php if ( $theme == $user->user_theme ) echo " checked ";?>/> <?php echo ucfirst ( $theme );?><br>
			<?php
			}
			
			/*			
				<input class="radio_switch" data-interact-id="css_custom" type="radio" name="theme" value="0"/> Custom			
			<br>			
			<a href="<?php echo site_url ('account/wysiwyg');?>">WYSIWYG editor</a>
			
			<!-- Css-->
			<section id="css_custom" class="content_hidden">							
				<label><?php echo lang ('edit your css');?></label>
				<textarea name="css"></textarea>
			</section>
			*/
			?>
			
			<input class="button button_big button_green" type="submit" value="<?php echo lang ('save changes');?>">
		</form>
		
	</div>
</div>
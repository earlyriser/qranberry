<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="<?php echo lang ('tagline');?>">

	<link rel="shortcut icon" href="<?php echo site_url ("favicon.ico");?>">

	<script src="<?php echo site_url ('assets/js/jquery-1.6.1.min.js');?>" type="text/javascript"></script>	
	<script src="<?php echo site_url ('assets/js/myplugin.js');?>" type="text/javascript"></script>	
	
	<?php if ( $view == "view_page" OR $view == "view_add" )
	{?>
		<meta name="viewport" content="width=device-width; initial-scale=1.0">		
		<title><?php  if ( isset ( $page->page_title ) ) echo $page->page_title;?></title>
		<!-- <script src="http://scripts.embed.ly/jquery.embedly.min.js"></script> -->
		<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css" media="screen" >
		<link rel="stylesheet" type="text/css" href="<?php echo site_url ('assets/css/common.css');?>" media="screen" >
		<link id="current_theme" rel="stylesheet" type="text/css" href="<?php echo site_url ('assets/css/themes/'.$user_theme.'/style.css');?>" media="screen" >

		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>		
		
	<?php 
	}
	else
	{?>
		<link rel="stylesheet" type="text/css" href="<?php echo site_url ('assets/css/style.css');?>" media="screen" >
		<link rel="stylesheet" type="text/css" href="<?php echo site_url ('assets/css/style_print.css');?>" media="print" >
		<link href='http://fonts.googleapis.com/css?family=Nothing+You+Could+Do' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" media="only screen and (max-width: 500px)" href="<?php echo site_url ('assets/css/small_device.css');?>" />	
		<title>Qranberry | <?php echo lang ('tagline');?></title>	
	<?php
	}
	
//JS VIEWS DEPENDENT
$js_calls = "";
switch ( $view )
{
	case 'view_account_themes':
		$js_calls .="$().showtoggle_radio()";
		break;
		
	case 'view_page':
		$js_calls .="$().qr_info();				
				$().showtoggle();
				$().hide_images();";
		break;
	default:
		$js_calls .="$().showtoggle();";
		break;
}
?>

<script type="text/javascript">
$(document).ready(function()
{
	<?php echo $js_calls;?>
});
</script>
	
</head>
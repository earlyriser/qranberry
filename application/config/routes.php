<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';


$route['id/(:any)'] = "page/view/$1";
$route['code/(:any)'] = "page/code/$1";

$route['account'] = "account";
$route['account/(:any)'] = "account/$1";

$route['site'] = "site";
$route['site/(:any)'] = "site/$1";

$route['section/(:any)'] = "section/index/$1";

$route['pages/(:any)'] = "site/pages/$1";

$route['paypal/(:any)'] = "paypal/$1";

$route['general/(:any)'] = "general/$1";

$route['task/(:any)'] = "task/$1";

$route['admin/(:any)'] = "admin/$1";

$route['es'] = "home/es";
$route['fr'] = "home/fr";
$route['en'] = "home";
$route['(:any)'] = "page/pro/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */
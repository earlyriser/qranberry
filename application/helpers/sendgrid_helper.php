<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ------------------------------------------------------------------------

//----------------------------------------------
//           GENERAL FUNCTIONS
//----------------------------------------------


if ( ! function_exists('send_email'))
{
	//----------------------------------------------
	function send_email ( $to, $subject, $html ) //str email, str subject, str html
	//----------------------------------------------
	{
		//instance
		$CI =& get_instance();

		//load
		$CI->load->library('email');
		
		$CI->email->initialize(array(				
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.sendgrid.net',
		  'smtp_user' => 'qranberry',
		  'smtp_pass' => 'g3ck0qr4nb!',
		  'smtp_port' => 587,
		  'crlf' => "\r\n",
		  'newline' => "\r\n"
		));
		
		$CI->email->from('info@qranberry.me', 'Qranberry');
		$CI->email->to( $to );
		$CI->email->subject( $subject );
		$CI->email->message( $html ); 
		$CI->email->send();
		
//		echo $CI->email->print_debugger();		
	}
}

//-----------------------------------------------
// EMAIL CONFIRMATION
//-----------------------------------------------
if ( ! function_exists('send_email_confirmation'))
{
	//----------------------------------------------
	 function send_email_confirmation ( $email, $user_token, $lang="en" )
	//----------------------------------------------
	{
		//load
		$CI =& get_instance();	
		$CI->lang->load('email', $lang);	
		
		//specs
		$subject = lang ('email_password_subject');
		$link = site_url ('general/login_url/'.$user_token);
		$content = str_replace ( '**LINK**', $link, lang('email_password_content') );

		send_email ( $email, $subject, $content );	
	}
}


//-----------------------------------------------
// SEND EMAIL PASSWORD
//-----------------------------------------------
if ( ! function_exists('send_email_password'))
{
	//----------------------------------------------
	 function send_email_password ( $email, $user_token, $lang="en" )
	//----------------------------------------------
	{
		//load
		$CI =& get_instance();	
		$CI->lang->load('email', $lang);	
	
		//specs
		$subject = lang ('email_password_subject');
		$link = site_url ('general/login_url/'.$user_token);
		$content = str_replace ( '**LINK**', $link, lang('email_password_content') );

		//send
		send_email ( $email, $subject, $content );	
	}
}
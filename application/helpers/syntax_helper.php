<?php

//-----------------------------------------------
function berrycode_to_html ( $string, $page )
//-----------------------------------------------
{
	class structure {};
	$structure = new structure();
	$structure->syntax = load_syntax ();
	$structure->page = $page;

	$structure->page->page_description = str_replace ( "\r\n[menu", "[menu", $structure->page->page_description );
	$structure->page->page_description = str_replace ( "\r\n[button", "[button", $structure->page->page_description );

	$structure->page->page_description = berrycode_lines ( $structure, 'link');
	$structure->page->page_description = berrycode_lines ( $structure, 'menu');
	$structure->page->page_description = berrycode_lines ( $structure, 'button');
 	$structure->page->page_description = berrycode_lines ( $structure, 'embed');
	$structure->page->page_description = berrycode_lines ( $structure, 'img');
	$structure->page->page_description = berrycode_lines ( $structure, 'header');
	
 	$structure->page->page_description = nl2br ( $structure->page->page_description );

	return $structure->page->page_description;
}

//-----------------------------------------------
function berrycode_lines ( $structure, $type )
//-----------------------------------------------
{
	$ok = false;
	$string = $structure->page->page_description;
	
	//while they are BC lines	
	while ( $ok == false )
	{
		//search line
		$result = start_end_str ($string, $structure->syntax->$type->start, $structure->syntax->$type->end );

		//if found, transform BC to html
		if  ( is_array ( $result ) )
		{		
			$length = $result['end'] - $result['start'];			
			$line =  substr ( $string , $result['start'], $length+1 );
			$new_line = transform_line ( $line, $structure, $type );
			$string = str_replace ( $line, $new_line, $string );
		}
		//not found, break loop
		else		
		{
			$ok = true;
		}
	}	
	return $string;
}


//-----------------------------------------------
function start_end_str ( $string, $start_str, $end_str)
//return array w start & end positions of a string,
//or false if it doesnt exist
//-----------------------------------------------
{
	$start = false;
	$end = false;
	
	//search start
	$start  = strpos ( $string, $start_str ) ;	
	//if start, search end
	if ( $start !== false )
		$end = strpos ( $string, $end_str, $start );
	//if both, return array
	if ( $start !== false AND $end !== false)
		return array ( 'start' => $start, 'end' => $end);
	else return false;
}


//-----------------------------------------------
function transform_line ( $line, $structure, $type )
//receive a berrycode line and return an html line
//-----------------------------------------------
{	
	$e = line_to_elements ( $line, $structure->syntax, $type );

	switch ( $type )
	{
		case "link":
			return " <a class='".$e['class']."' href=\"".$e['url']."\">".$e['text']."</a>";
		case "menu":
			return " <a class='menu' href=\"".$e['url']."\">".$e['text']."</a>";
		case "button":
			return " <a class='button' href=\"".$e['url']."\">".$e['text']."</a>";		
		case "embed":
			return " <a class='embed_content' href=\"".$e['url']."\">".$e['url']."</a>";
		case "img":
			return transform_img ($e, $structure);
		case "header":
			return  transform_header ($e);
	}
	return $line;
}

function transform_header ( $e )
{
	if ( ! $e['level'] ) $e['level'] = 1;
	return  "<h".$e['level'].">".$e['text']."</h".$e['level'].">";
}

//-----------------------------------------------
function transform_img ( $e, $structure )
//receive line elements array, and structure obj
//returns ready to use img line
//-----------------------------------------------
{	
	$photos =explode (",", $structure->page->page_photos);
	
	//src
	if ( is_numeric ( $e['src'] ) )
	{
		$src = site_url ('assets/uploads/'.$structure->page->page_id.'_'.$photos[ $e['src']-1 ].'.jpg');
	}
	else
		$src = $e['src'];

	//href
	if ( $e['url'] != '' )
		$href = $src;
	else
		$href= $e['url'];

	return "<a href='".$href."'><img class='image pasted_image' src='".$src."'></a>";
}

//-----------------------------------------------
function line_to_elements ( $line, $syntax, $type )
//break in elements a berrycode line, returning associative array
//return ex: array ( 'url'=> string, 'text' => string, 'type' => string )
//-----------------------------------------------
{
	$result = array ();
	
	//cut start and end form line
	$line = str_replace ( array ( $syntax->$type->start, $syntax->$type->end ),  array ( "", "" ) , $line );
	
	//explode & trim
	$strings = explode ( "|", $line );
	$counter = 0;
	foreach ( $syntax->$type->elements as $element )
	{		
		if ( isset ( $strings [ $counter ] ) )
			$result [ $syntax->$type->elements[ $counter ] ] = trim ( $strings [ $counter ] );
		else
			$result [ $syntax->$type->elements[ $counter ] ] = false;
		$counter++;
	}
	
	return $result;
}


//-----------------------------------------------
function load_syntax ()
//-----------------------------------------------
{
	$jsonurl = site_url ('assets/js/berrycode.json');
	$json = file_get_contents($jsonurl,0,null,null);
	return json_decode($json);
}

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ------------------------------------------------------------------------

//----------------------------------------------
//           GENERAL FUNCTIONS
//----------------------------------------------

if ( ! function_exists('qr_show_image'))
{
	//----------------------------------------------
	function qr_show_image ( $qr_url, $qr_file_path, $qr_google_path, $format, $formats_path ) 
	//----------------------------------------------
	{
		if ( !file_exists($qr_file_path)) 
		{
			$file_exist = copy($qr_google_path, $qr_file_path );	
		}
		else
			$file_exist = true;
		
		if ( $file_exist )
			qr_make_composite ( $format, $formats_path, $qr_file_path);
		
	}
}

if ( ! function_exists('qr_make_composite'))
{
	//----------------------------------------------
	function qr_make_composite ( $format, $formats_path, $qr_file_path ) 
	//----------------------------------------------
	{
		//params
		$strs = explode ( '-', $format);
		$format_name = $strs[0];
		$format_lang = $strs[1];

		$json = file_get_contents($formats_path,0,null,null);
		$formats = json_decode($json);
		$format_obj = $formats->$format_name;		
		$img_bg = imagecreatefromjpeg( site_url ('assets/img/pdf/'.$format.'.jpg') );
		$img_over = imagecreatefrompng( $qr_file_path );
		$over_w = imagesx ( $img_over );
		
		//resize img over
		$new_image = imagecreatetruecolor( $format_obj->w, $format_obj->h);
		imagecopyresampled($new_image, $img_over, 0, 0, 0, 0, $format_obj->w, $format_obj->h, $over_w , $over_w );
		$img_over = $new_image;		
	  
		//positionate images over
		$elements = $formats->$format_name->elements;
		foreach ( $elements as $element )
		{
			imagecopymerge($img_bg, $img_over, $element->x, $element->y, 0, 0, $format_obj->w, $format_obj->h, 100);		
		}
		
		//show img on browser
		header('Content-Type: image/jpeg');
		imagejpeg($img_bg);

		//clean
		imagedestroy($new_image);
		imagedestroy($img_bg);
		imagedestroy($img_over);
	
	}
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('validate_theme'))
{
	function validate_theme($themes, $theme )
	{
		if ( $theme )
		{
			if ( array_key_exists ( $theme, $themes) )
				return $theme;
			else
				return 0;
		}
	}
}


/* End of file download_helper.php */
/* Location: ./system/helpers/download_helper.php */
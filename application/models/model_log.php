<?php
class Model_log extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	//----------------------------
    function insert_log( $data )
	//----------------------------
    {
		$this->db->insert('logs', $data);
    }
	
	//----------------------------
    function get_logs ( $param, $order, $limit )
    //----------------------------
	{
        //table
		$this->db->from  ('logs');
		
		//parameters
		if ( $param )
		{
			foreach ( $param as $key => $value )
			{	$this->db->where ($key, $value );		}	
		}		
		//order
		if ( $order )
		{
			foreach ( $order as $key => $value )
			{	$this->db->order_by($key, $value); 		}
		}
		//limit
		if ( $limit )
		{	$this->db->limit($limit); 		}		
		
		//get
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
		{
			if ( $limit == 1 )
				return $query->row();
			else
				return $query->result();
		}
		else
			return false;
    }
}
<?php
class Model_theme extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	
    function get_themes( $ids )
    {
        //table
		$this->db->from  ('themes');
		$this->db->where_in('theme_id', $ids);
		
		//get
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
			return $query->result();
		else
			return false;
    }

}
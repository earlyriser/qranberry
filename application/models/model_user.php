<?php
class Model_user extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	//-----------------------------------
	function update_user ( $data )
	{
		$this->db->where('user_id', $data['user_id']);		
        $this->db->update('users', $data);	
	}
	
	//-----------------------------------
    function get_user ( $param, $order, $limit )
    {
        //table
		$this->db->from  ('users');
		
		//parameters
		if ( $param )
		{
			foreach ( $param as $key => $value )
			{	$this->db->where ($key, $value );		}	
		}		
		//order
		if ( $order )
		{
			foreach ( $order as $key => $value )
			{	$this->db->order_by($key, $value); 		}
		}
		//limit
		if ( $limit )
		{	$this->db->limit($limit); 		}		
		
		//get
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
		{
			if ( $limit == 1 )
				return $query->row();
			else
				return $query->result();
		}
		else
			return false;			

    }

	//-----------------------------------
	function update ( $data )
	{
		$this->db->where('user_id', $data['user_id']);		
        $this->db->update('users', $data);
	}
	
	//----------------------------
    function insert( $data )
	//----------------------------
    {
		$this->db->insert('users', $data);
    }	
}
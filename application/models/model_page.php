<?php
class Model_page extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	//----------------------------
    function insert_page ( $data )
	//----------------------------
    {
		$this->db->insert('pages', $data);
    }

	//----------------------------
	function delete ( $page_id )
	//----------------------------
	{
		$this->db->delete('pages', array( 'page_id' => $page_id )); 
	}
	
	//----------------------------
    function update ( $data )
	//----------------------------
    {
		$this->db->where('page_id', $data['page_id']);		
        $this->db->update('pages', $data);
    }	
	
	//----------------------------
    function get_pages ( $param, $order, $limit )
    //----------------------------
	{
        //table
		$this->db->from  ('pages');
		
		//parameters
		if ( $param )
		{
			foreach ( $param as $key => $value )
			{	$this->db->where ($key, $value );		}	
		}		
		//order
		if ( $order )
		{
			foreach ( $order as $key => $value )
			{	$this->db->order_by($key, $value); 		}
		}
		//limit
		if ( $limit )
		{	$this->db->limit($limit); 		}		
		
		//get
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
		{
			if ( $limit == 1 )
				return $query->row();
			else
				return $query->result();
		}
		else
			return false;
    }

	//----------------------------
	function get_page_user ( $id )
	//----------------------------
	{
		$this->db->from  ('pages');
		$this->db->join('users', 'users.user_id = pages.user_id');
		$this->db->where ( 'page_id', $id );		
		$this->db->limit( 1 ); 	
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
			return $query->row();
		else
			return false;	
	}
}
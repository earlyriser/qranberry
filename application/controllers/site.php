<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->model('Model_page');
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);
		
		$this->load->helper('account');		
	}
	
	//------------------------
	public function upload()
	//------------------------
	{
		$timestamp = time();
		//config upload
		$user_id = $this->session->userdata('user_id');
		$config_upload['upload_path'] = $this->config->item ('upload_user_path' ).$user_id.'/';
		$config_upload['allowed_types'] = 'jpg|png';
		$config_upload['max_size']	= '4000';
		//$config_upload['file_name'] = $this->session->userdata ( 'page_id' ).'_'.$timestamp.'.jpg';
		$config_upload['overwrite'] = true;

		//create folder
		if(!file_exists( $config_upload['upload_path'] )) 
		{	mkdir( $config_upload['upload_path'], 0777);} 

		//upload
		$this->load->library('upload', $config_upload);
		
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			var_dump ( $error );
			echo "error";
		}
		else
		{
			//rename
			$upload_data = $this->upload->data();
			$upload_data['perm_path'] = $config_upload['upload_path'].$timestamp.$upload_data['file_ext'];
			rename ( $upload_data['full_path'], $upload_data['perm_path'] );
			
			//config resize			
			$config_resize['image_library'] = 'gd2';
			$config_resize['source_image'] = $upload_data['perm_path'];
			$config_resize['width'] = 600;
			$config_resize['height'] = 4000;
			$config_resize['maintain_ratio'] = TRUE;
			
			//resize
			$this->load->library('image_lib', $config_resize);
			$resize_ok = $this->image_lib->resize();
	
			if ( $resize_ok )
			{
				$result= array ( 'status'=>'ok', 'path' =>  site_url ( substr ( $upload_data['perm_path'], 2 ) ) );?>
				<div id="response"><?php echo json_encode ( $result );?></div>
			<?php
			}
		}
	}
	
	//------------------------
	private function _get_images ( $user_id )
	//------------------------
	{
		$this->load->helper('directory');
		$result = directory_map( $this->config->item ('upload_user_path' ).$user_id.'/' ); 		
		return $result;
	}
	
	//------------------------
	public function add ()
	//------------------------
	{
//$this->output->enable_profiler(TRUE);	
//echo "YOU NEED TO BE LOGGED";
		logornot();
			
		//LOADS
		$this->load->library('form_validation');
		$this->load->helper('string');
		$this->load->model('Model_user');
		
		//VARS
		$page_id = $this->_assign_id();
		$user_id = $this->session->userdata('user_id');
		$page_db = $this->Model_page->get_pages ( array ( 'page_id' => $page_id), false, 1 );
		$page = $this->_output_elements ( $page_db );
		$user = $this->Model_user->get_user( array ('user_id' => $user_id ), false, 1);
		$images = $this->_get_images ( $user_id );
		$themes = array_merge ( $this->config->item( 'gratis_themes'), $this->config->item( 'pro_themes'),  $this->config->item( 'enterprise_themes') );
		$pages = $this->Model_page->get_pages ( array ( 'user_id' => $user_id ), array( 'page_title' => 'DESC' ), false);

		
		//validation settings
		$this->form_validation->set_rules('page_title', 		lang('title') 		);
		$this->form_validation->set_rules('page_description', 	lang('content'), 	'required');

		//validation BAD
		if ($this->form_validation->run() == FALSE)
		{
			//view	
			$data['images']		= $images;
			$data['view'] 		= 'view_add';
			$data['user']		=$user;
			$data['themes']		= $themes;
			$data['pages']		= $pages;
			$data['user_theme'] = $user->user_theme;
			$data['id'] 	= $page_id;
			$data['page'] = $page;
			$data['page_db'] = $page_db;
			$this->load->view('view_template', $data);				
		}
		else //validation OK
		{
			//pipeline page
			$this->_pipeline_page ( $page_id, $page_db, $user_id );
			//update theme			
			$this->_change_theme ( $user );
			//flashdata
			$this->session->set_flashdata('alert', 'msg_page_added');			
			//redirect 
			redirect ('id/'.$page_id);
		}
	}
	
	//------------------------------------------
	private function _change_theme ( $user )
	//------------------------------------------
	{
		$theme_class = 'gratis';
		$user_theme = $this->input->post('user_theme');
		
		//check to which theme class the selected theme belongs
		if ( in_array ( $user_theme, $this->config->item( 'pro_themes') ) )
			$theme_class = 'pro';
		if ( in_array ( $user_theme, $this->config->item( 'enterprise_themes') ) )
			$theme_class = 'enterprise';
		
		//if plan has access to this theme, update
		if ( $theme_class == 'gratis' OR $theme_class == $user->user_plan OR $user->user_plan == 'enterprise' )
			$this->Model_user->update ( array ( 'user_id' => $user->user_id, 'user_theme' => $user_theme) ); 	
	}
	
	//------------------------------------------
	private function _pipeline_page ( $id, $page_db, $user_id )
	//------------------------------------------
	{
		$time = date('Y-m-d H:i:s'); //0000-00-00 00:00:00
		//make data
		$data['page_id'] = $id;
		$data['page_title'] = $this->input->post('page_title');
		$data['page_description'] = $this->input->post('page_description');
		$data['user_id'] = $user_id;
		$data['page_photos'] = $this->input->post('page_photos');
		
		if ( $page_db )	//update
		{
			$data['page_updated'] = $time;
			$this->Model_page->update ( $data );
		}
		else				//insert
		{
			$data['page_created'] = $time;
			$data['page_updated'] = $time;
			$this->Model_page->insert_page ( $data );
		}
	}
	
	//----------------------------------------
	private function _assign_id ( $force_new = false )
	//----------------------------------------
	{
		if ( $force_new OR $this->session->userdata ( 'page_id' ) == false )
		{
			$string = strtolower ( random_string('alpha', 6) );
			$this->session->set_userdata( array ( 'page_id' => $string ) );
			return $string;
		}
		else
			return $this->session->userdata ( 'page_id' ) ;			
	}
	

	//----------------------------------------
	function _output_elements ( $object_db )
	//----------------------------------------
	//returns an array to ouput some element in a form, 
	{				
		$fields = array ( 'page_id', 'page_title', 'page_description', 'page_photos');
		
		$elements = array ();
		
		foreach ( $fields as $field )
		{
			$elements[ $field ] ='';
			
			if ( $object_db != false AND $object_db->$field != '' )
			{
				$elements[ $field ] = $object_db->$field ; 
			}
			if ( $_POST )
			{
				$elements[ $field ] = $this->input->post( $field ); 
			}
		}
		if ( $_POST ) 
			$elements['page_email'] = $this->input->post( 'page_email' );
		else
			$elements['page_email'] = '';
		
		return $elements;
	}
	
	function create ()
	{
		$force_new = true;
		$this->_assign_id ( $force_new );
		redirect ( 'site/add' );
	}

	function pages ( $view )
	{
		$data['view'] = $view;
		
		$this->load->view('view_meta', $data);
		$this->load->view('view_header');
		$this->load->view('pages/'.$view);
		$this->load->view('view_footer'); 
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
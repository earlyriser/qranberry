<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);
	}
	 
	public function index( $param )
	{		
		if ( method_exists( $this, $param ) )
		{}
		else
			$this->view ( $param );	
	}

	
	public function view ( $section )
	{
		//view	
		$data['view'] = 'pages/view_'.$section;			
		$this->load->view('view_template', $data);		
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
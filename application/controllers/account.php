<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);
		
		$this->load->model('Model_user');
		$this->load->helper('account');
	}
	
	//-------------------------------------
	public function index()
	//-------------------------------------
	{		
		//loads
		$this->load->model('Model_page');

		//vars
		$logged = $this->session->userdata('logged_in');
		$user = false;
		$pages = false;
	
		//logged?
		logornot();
	
		//queries
		$user = $this->Model_user->get_user ( array ('user_id' => $this->session->userdata('user_id') ) , false, 1 );
		if ( $user )
		{
			$pages = $this->Model_page->get_pages ( array ( 'user_id' => $user->user_id ), array( 'page_updated' => 'DESC' ), false);
		}

		
		//view	
		$data['view'] = 'view_account';
		$data['user'] = $user;
		$data['pages'] = $pages;
		$this->load->view('view_template', $data);

	}

	//-------------------------------------
	public function delete ()
	//-------------------------------------
	{
		//loads
		$this->load->model('Model_page');
		
		//logged?
		logornot();
		
		//vars
		$page_id = $this->input->post( 'id' );				
		$user_id  = $this->session->userdata('user_id');
		$page = false;
		
		//if vars ok, make query
		if ( is_string( $page_id )  AND $user_id )
			$page = $this->Model_page->get_pages ( array ( 'page_id' => $page_id ), false, 1 );

		//if page found & belongs to user, delete page & make log
		if ( $page AND $page->user_id == $user_id )
		{
			$this->Model_page->delete ( $page_id );
		}
		
		redirect ('account');
	}	
	
	//-------------------------------------
	public function printing( $page_id, $format=false)
	//-------------------------------------
	{
		if ( !$format )
		{
			//view	
			$data['page_id'] = $page_id;
			$data['view'] = 'view_account_printing';
			$this->load->view('view_template', $data);
		}
		else
		{
			//load
			$this->load->helper('qr');
			
			//vars
			$qr_url = site_url ( '/id/'.$page_id );
			$qr_file_path = $this->config->item('qr_file_path').$page_id.'.jpg';
			$qr_google_path = str_replace ( array( 'TOKEN_WIDTH'), array(300), $this->config->item('qr_google_path') ).$qr_url;
			$formats_json = site_url ( $this->config->item ( 'qr_formats_print' ) );
						
			qr_show_image ( $qr_url, $qr_file_path, $qr_google_path, $format, $formats_json );
		}
	}

	//-------------------------------------
	public function mobile_link ( $page_id, $format=false )
	//get mobile link image and opens a download dialog
	//-------------------------------------
	{	
		$url = site_url ( 'account/printing/'.$page_id.'/'.$format);
		header('Content-Type: image/jpeg');
		header('Content-Disposition: attachment;filename="'.$format.'.jpeg"');
		$fp=fopen($url,'r');
		fpassthru($fp);
		fclose($fp);
	}
	
	
	//-------------------------------------
	public function profile()
	//-------------------------------------
	{
		//loads
		$this->load->library('SimpleLoginSecure');	

		//vars
		$user_id = $this->session->userdata('user_id');
		$password = $this->input->post( 'password' );
		$user_name = $this->input->post( 'user_name' );
		
		//logged?
		logornot();
		
		//queries
		$user = $this->Model_user->get_user ( array ('user_id' => $user_id ) , false, 1 );
				
		//if something submitted
		if ( $user AND ( $password OR $user_name) )
		{	
			$data_user['user_id'] = $user->user_id;
			
			//username ok?
			if ( $this->_user_name_validation ( $user_name ) )
			{
				$data_user['user_name'] = $user_name;
				$update = true;
			}
			
			//password ok?
			if ( $password AND $password !="" )
			{
				$data_user['user_pass'] = $this->simpleloginsecure->make_hash ( $password );
				$update = true;
			}
			
			//update user	
			if ( isset ( $update ) )
			{
				$this->Model_user->update_user ( $data_user );

				//flashdata
				$this->session->set_flashdata('alert', 'msg_password_changed');	

				//redirect
				redirect ('account');				
			}
		}		
		
		//view	
		$data['view'] = 'view_account_profile';
		$data['user'] = $user;
		$this->load->view('view_template', $data);
		
	}

	private function _user_name_validation ( $user_name )
	{
		//if user_name chars are ok, search if it's not already taken in DB
		if ( preg_match ("/^([abcdefghijklmnopqrstuvwxyz0-9_])+$/i", $user_name ))
		{
			if ( $this->Model_user->get_user ( array ( 'user_name' => $user_name ), false, 1) )
				return false;
			else
				return true;
		}
		else
		{
			return false;
		}

	}
	
	//-------------------------------------
	public function plans()
	//-------------------------------------
	{
		//logged?
		logornot();	

		//vars
		$data['user_id']= $this->session->userdata('user_id');
		$data['user'] 	= $this->Model_user->get_user ( array( 'user_id' => $data['user_id']), false, 1 );
		
		//interrupt plan
		if ( $this->input->post( 'interrupt' ) )
		{
			//demote
			$this->Model_user->update_user( array ( 'user_id'=> $data['user_id'], 'user_plan' => 'gratis' ) );
			//flashdata
			$this->session->set_flashdata('alert', 'msg_plan_updated');			
			//redirect 
			redirect ('account');
		}
		
		//view	
		$data['view'] = 'view_account_plans';
		$this->load->view('view_template', $data);
		
	}
	
	//-------------------------------------
	public function edit ( $page_id = false )
	//-------------------------------------
	{
		//loads
		$this->load->model('Model_page');
		
		//vars
		$user_id = $this->session->userdata('user_id');
		$page = false;
		
		//find page
		if ( $page_id AND $user_id )
		{			
			$page = $this->Model_page->get_pages ( array ( 'page_id' => $page_id, 'user_id' => $user_id ), false, 1 );
		}
		
		//if page, put in session
		if ( $page )
		{
			$this->session->set_userdata( array ( 'page_id' => $page_id ) );
		}
		
		redirect ('site/add');
	}

	
	//-------------------------------------
	public function set_home ()
	//-------------------------------------
	{
		//loads
		$this->load->model('Model_page');
		
		//logged?
		logornot();
		
		//vars
		$page_id = $this->input->post( 'homepage' );				
		$user_id  = $this->session->userdata('user_id');
		
		//if vars ok, make query
		if ( is_string ( $page_id )  AND $user_id )
			$this->Model_user->update( array ( 'user_id' => $user_id, 'user_home' => $page_id ) );
		
		redirect ('account');
	}	
	
	//-------------------------------------
	public function stats ()
	//-------------------------------------
	{
		//logged?
		logornot();
				
		//load
		$this->load->model('Model_stat');
		
		//vars
		$user_id  = $this->session->userdata('user_id');	
		$now = gmdate('Y-m-d H:m:s');
		$range = 30;
 		$lastdays = date ( 'Y-m-d H:m:s', strtotime ( $now ." - ".$range." days") );		
		
		//query get stats from last 30 days
		$stats = $this->Model_stat->get_stats ( array ('user_id'=> $user_id, 'stat_timestamp >=' => $lastdays, 'stat_timestamp <' => $now ), array ('stat_timestamp'=>'asc'), false );
		
		//view
		$data['range'] = $range;
		$data['stats'] = json_encode( $this->_str_timestamps( $stats ) );
		$data['view'] = 'view_account_stats';
		$this->load->view('view_template', $data);		
	}
	
	//-------------------------------------
	private function _str_timestamps ( $array )
	//input: y-m-d h:m:s -------> y-m-d
	//-------------------------------------
	{
		foreach ( $array as $element){
			$element->stat_timestamp = substr( $element->stat_timestamp, 0, 10) ; 	
		}
		return $array;
	}
		
}




/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
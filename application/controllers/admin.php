<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->load->model('Model_user');
	}
	
	public function check ( )
	{
		$this->load->helper('language_checker');
		$this->load->helper('directory');
		$struct  = directory_map('./application/language');     
		echo compare_language_files ( $struct );
	}
	
	public function last ()
	{
		$this->load->model('Model_page');
		
		$pages = $this->Model_page->get_pages (array(), array ('page_updated' => 'desc' ), 30 );
		
		//view	
		$data['view'] = 'view_admin_last';
		$data['pages'] = $pages;
		$this->load->view('view_template', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
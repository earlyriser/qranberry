<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function _remap($method)
	{
	  if (method_exists($this, $method))
		$this->$method();
	  else 
		$this->index($method);
	}
	 
	//------------------------
	public function index ( $lang='en' )
	//------------------------
	{
		//lang
		if ( !in_array ( $lang, $this->config->item('langs') ) )
			$lang = 'en';
		$this->lang->load('general', $lang);			
		$this->session->set_userdata('lang', $lang);
		
		//view	
		$data['view'] = 'view_home';
		$this->load->view('view_meta', $data);
		$this->load->view('view_home', $data);		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
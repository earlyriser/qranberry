<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->model('Model_page');
		$this->load->helper('syntax');			
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);		
	}


	public function pro ( $user_name, $page_id = false )
	{
		//loads
		$this->load->model('Model_user');
		$user = false;
		
		if ( $user_name )
		{
			$user = $this->Model_user->get_user( array ('user_name' => $user_name ), false, 1);			
			$this->view ( $user->user_home );
		}
		
	}
	
	//----------------------------
	public function view ( $id )
	//----------------------------
	{
		//get page
		$page = $this->Model_page->get_page_user ( $id );
	
		//view
		if ( !$page )
		{
			show_404();
		}
		else
		{			
			$data['page'] = $page;				
			$data['view'] 		= 'view_page';
			$data['user_theme'] = $page->user_theme;
			$data['photos']		= explode (",", $page->page_photos);
			$this->load->view('view_template', $data);			
		
			//stats
			$this->load->model('Model_stat');
			$this->Model_stat->insert ( array ( 'stat_id'=>random_string('unique'), 'stat_ip'=>$this->input->ip_address(), 'stat_timestamp'=>gmdate('Y-m-d H:m:s'), 'user_id'=>$page->user_id, 'page_id'=>$id ) );			
		}
	}
	
	//----------------------------
	public function code ( $id )
	//----------------------------
	{
		//output
		$data['page'] = $this->Model_page->get_pages ( array ( 'page_id' => $id), false, 1 );
		
		//view				
		$data['view'] 		= 'view_page_code';
		$this->load->view('view_template', $data);			
	}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
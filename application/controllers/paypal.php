<?php
/**
 * PayPal_Lib Controller Class (Paypal IPN Class)
 *
 * Paypal controller that provides functionality to the creation for PayPal forms, 
 * submissions, success and cancel requests, as well as IPN responses.
 *
 * The class requires the use of the PayPal_Lib library and config files.
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Commerce
 * @author      Ran Aroussi <ran@aroussi.com>
 * @copyright   Copyright (c) 2006, http://aroussi.com/ci/
 *
 */

class Paypal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Paypal_Lib');
		$this->load->helper('sendgrid');
	}

	function index ()
	{
		echo " ";
	}
	
	function ipn()
	{
		// You can access a slew of information via the ipn_data() array. 

		//VARS
		$to    = 'romama@gmail.COM';    //  your email
		$body ="";
		$subject ="PayPal IPN";
		
		$item_name = false;
		$mc_gross = false;
		$mc_currency = false;
		$payment_status = false;
		$txn_type = false;
		$user_id = false;		
			
		//loads
		$this->load->model('Model_user');	
		$this->load->library('email');
	
		//if IPN is valid
		if ( $this->paypal_lib->validate_ipn() )
		{
			$body ='GOOOD';

			//GET IPN VALUES
			if ( isset ( $ipn_data['custom'] ) )		$user_id =  int ( $ipn_data['custom'] );			//user_id
			
			//if it's a new signup /or maybe Im just getting payments, need to check that
			if ( is_plan_ok ( $ipn_data ) )
			{
				$this->Model_user->update_user( array ( 'user_id'=> $user_id, 'user_plan' => is_plan_ok ( $ipn_data ) ) );
			}
		}
		
		foreach ($_POST as $field=>$value)
		{      
			$body .= "\n$field: $value";
		}		
		
		//prepare and send mail
		if ( $body != "" )
			send_email ( $to, $subject, $body );	

	}
	
	function is_plan_ok ( $ipn_data )
	{
		$result = false;
		
		if ( 	$ipn_data['txn_type'] 	== "subscr_payment" AND 
				$ipn_data['item_name'] 	== 'pro_monthly' AND 
				$ipn_data['mc_gross']	== "9" AND 
				$ipn_data['mc_currency']== "USD" AND 
				$ipn_data['payment_status']=="Completed" )
			return "pro";
		
		if ( 	$ipn_data['txn_type'] 	== "subscr_payment" AND 
				$ipn_data['item_name'] 	== 'enterprise_monthly' AND 
				$ipn_data['mc_gross']	=="24" AND 
				$ipn_data['mc_currency']=="USD" AND 
				$ipn_data['payment_status']=="Completed" )
			return "enterprise";
			
		return $result;
	}

}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);	
		$this->load->library('SimpleLoginSecure');		
	}

	//------------------------
	function _remap($method)
	//------------------------
	{
	  if (method_exists($this, $method))
		$this->$method();
	}
	

	//-------------------------------------
	public function logout (  )
	//-------------------------------------
	{		
		//logout
		$this->simpleloginsecure->logout();
		//redirect	
		redirect ();
	}
	
	//-------------------------------------
	public function login ()
	//-------------------------------------
	{		
		//redirect to account if logged
		if ($this->session->userdata('logged_in') )
			redirect ('account');
		
		//vars
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		
		//login ok
		if ( $this->simpleloginsecure->try_login( $email, $password ) )
		{
			redirect ('account');
		}
		else
		{			
			//view
			$data['email'] 	= $email;
			$data['view'] 	= 'view_login';
			$this->load->view ('view_template', $data);	
		}
	}

	//-------------------------------------
	public function signup ()
	//-------------------------------------
	{		
		//loads
		$this->load->model('Model_user');
		$this->load->library('form_validation');
		$this->load->helper('string');
		$this->load->helper('sendgrid');
		
		//redirect to account if logged
		if ($this->session->userdata('logged_in') )
			redirect ('account');
		
		//vars
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$user_token = strtolower ( random_string('alpha', 32 ));
		
		$this->form_validation->set_error_delimiters('<div class="form_error">', '</div>');
		$this->form_validation->set_rules('password', 	lang('password'), 	'required');
		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email|callback_email_check');
		
		//error
		if ($this->form_validation->run() == FALSE)
		{
			//view
			$data['email'] 	= $email;
			$data['view'] 	= 'view_signup';
			$this->load->view ('view_template', $data);	
		}
		//ok
		else
		{
			$this->Model_user->insert( array ( 	'user_email'	=>$email, 
												'user_pass'		=> $this->simpleloginsecure->make_hash ($password), 
												'user_theme'	=>'standard',
												'user_plan'		=>'gratis',
												'user_token' 	=>$user_token,
												'user_date' 	=>	date('c'),
												'user_modified' =>	date('c'),
												'user_last_login'=> date('c')
												) );
			$user_id = $this->db->insert_id();
			$this->simpleloginsecure->login ( $user_id );
			
			send_email_confirmation ( $email, $user_token );
			redirect ('account');
		}
	}

	//-------------------------------------
	public function email_check ($str)
	//-------------------------------------
	{
		$user_exists = $this->Model_user->get_user ( array( 'user_email'=>$str ), false, 1);
		if ($user_exists)
		{
			$this->form_validation->set_message('email_check', 'This email is already taken');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	//-------------------------------------
	public function login_url ( )
	//-------------------------------------
	{
		$token = $this->uri->segment(3);
		
		//load	
		$this->load->model('Model_user');

		//vars
		$user = false;
		
		//queries
		if ( $token != '' )
		$user = $this->Model_user->get_user ( array ( 'user_token' => $token ), false, 1 ); //param, order, limit
		
		//ok
		if ( $user )
		{
			$this->simpleloginsecure->login ( $user->user_id );			
			$this->session->set_flashdata('alert', 'msg_login_url');
			redirect ('account/profile');
		}
		//error
		else
		{
			redirect ('general/login');
		}
	}
	
	//-------------------------------------
	public function password_reminder ()
	//-------------------------------------
	{	
		//loads
		$this->load->model('Model_user');
		$this->load->helper('sendgrid');
		
		//vars
		$email 	= $this->input->post('email');
		$user 	= false;

		//query
		if ( $email )
		$user = $this->Model_user->get_user ( array ( 'user_email' => $email ), false, 1 );

		if ( $user )
		{
			//send email
			send_email_password ( $user->user_email, $user->user_token, $this->mylang  );

			//flashdata
			$this->session->set_flashdata('alert', 'msg_password_sent');
			
			redirect ('general/login');
		}
		else
		{
			//view
			$data['view'] 	= 'view_password_reminder';
			$this->load->view ('view_template', $data);	
		}
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
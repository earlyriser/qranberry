<?php

//PASSWORD
$lang['email_password_subject'] = "Tu contrase�a";

$lang['email_password_content']		= <<<EOM
Recibes este email porque has pedido reajustar la contrase�a de tu cuenta Qranberry. 

Por favor, ve al enlace siguiente y elige una nueva contrase�a:
**LINK**

Gracias y hasta la pr�xima.
El Equipo Qranberry.
EOM;
//-------------------------------


//CONFIRMATION
$lang['email_confirmation_subject'] = 'Confirmation email';

$lang['email_confirmation_content']	 = <<<EOM
Te damos la bienvenida a Qranberry!
	
Visita este enlace para confirmar que posees este email:
**LINK**

Si recibes este mensaje pro error, simplemente b�rralo.
	
Muchas gracias,
El Equipo Qranberry
EOM;
//------------------------------


//PRO SUBSCRIBE
$lang['email_pro_subscribe_subject'] = 'Your Pro Account';

$lang['email_pro_subscribe_content'] = <<<EOM

Hola.

Tu nueva cuenta Pro est� lista.

Hay 3 cosas que probablemente quieras hacer en tu Cuenta
1. Seleccionar un nombre de usuario que har� tu sitio accesible desde qranberry.me/nombre_de_usuario
2. Configurar una p�gina de inicio
3. Elegir un tema

Si tienes cualquier duda o problema, escr�benos.

Gracias y hasta la pr�xima.
El Equipo Qranberry.
EOM;

//--------------------------------




$lang['email_plan_unsubscribe'] ="";

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */
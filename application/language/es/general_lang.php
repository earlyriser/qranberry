<?php
 
//FLASH
$lang['msg_password_changed'] 	= "Tu contraseña ha sido modificada";
$lang['msg_page_added']		= "<b>Esta es tu página.</b><br>1. <a href='*URL_ADD*'>Edítala</a> si es necesario.<br>2. Imprime y pega tu <a href='*URL_QR*'>código QR</a><br>3. Eso es todo.";
$lang['msg_password_sent'] 	= "Checa tu correo (y el folder de spam). Encontrarás un enlace para modificar tu contraseña.";
$lang['msg_login_url']		= "Hola. Por favor cambia tu contraseña.";
$lang['msg_theme_updated'] = "Tu tema ha sido actualizado.";
$lang['msg_plan_updated'] = "Tu plan ha sido actualizado.";

//Common
$lang['current_lang'] = "es";
$lang['account'] = "Cuenta";
$lang['login'] = "Conectarse";
$lang['logout'] = "Desconectarse";
$lang['password'] = "Contraseña";
$lang['email'] = "Email";
$lang['submit'] = "Enviar";
$lang['tagline'] = "Crea tu sitio móvil y códigos QR";
$lang['cancel'] = "Cancelar";
$lang['delete'] = "Borrar";
$lang['signup'] = "Registrarse";

//account
$lang['create page'] = "Crear página";
$lang['change password'] = "Cambiar contraseña";
$lang['change plan'] = "Cambiar plan";
$lang['pages'] = "Páginas";
$lang['edit'] = "Editar";
$lang['profile'] = "Perfil";
$lang['username'] = "Nombre de usuario";
$lang['homepage'] = "Página de inicio";
$lang['change homepage'] = "Cambiar página de inicio";
$lang['the homepage is what people will see at'] ="La página de inicio estará disponible en";
$lang['do you really want to delete this page?'] ="Realmente deseas borrar esta página?";
	
//account profile
$lang['change your password'] = "Cambia tu contraseña";
$lang['forgot it'] = "¿La olvidaste?";
$lang['choose wisely'] = "Escógelo bien, porque será permanente.";
$lang['your site will be at'] = "Tu sitio estará en:";
$lang["my_username"] = "mi_nombre_de_usuario";

//account printing
$lang["printing"] = "Imprimir";
$lang["print step 1"] = "Descarga to enlace móvil";		
$lang["print step 2"] = "Imprímelo";	
$lang["print step 3"] = "Úsalo en tus productos o tu negocio";	
$lang["ml resto"] = "Enlaces móviles para restaurantes";	
$lang["ml sale"] = "Enlaces móviles para venta";	
$lang["ml rent"] = "Enlaces móviles para renta";	
$lang["ml perso"] = "Enlace móvil personalizado";	
$lang["qr standalone"] = "Simple código QR";	
$lang["download and integrate"] = "Bájalo e intégralo en tu diseño con tu editor de imágenes preferido.";	
		
//password reminder
$lang['get your password'] = "Recupera tu contraseña";

//page
$lang['qr code'] = "Código QR";
$lang['print qr code'] = "Imprimir código QR";

//footer
$lang['footer phrase'] = "Página móvil hecha con ";
$lang['TOS'] = "Términos de servicio";
$lang['PP'] = "Política de privacidad";
$lang['contact'] = "Contacto";

//page code
$lang['your title'] = "Tu título";

//plans
$lang['plans'] = "Planes";
$lang['your current plan'] = "Tu plan actual:";
$lang['free'] = "GRATIS";
$lang['monthly'] = "por mes";
$lang['became Pro'] = "Volverse Pro";
$lang['interrupt plan'] = "Cancelar plan";
$lang['signup_plan'] = "Subscribirse";

//style
$lang['style'] = "Estilo";
$lang['edit your css'] = "Edita tu CSS";
$lang['save changes'] = "Guardar cambios";
$lang['themes'] = "Temas";

//add
$lang['create your mobile page'] = "Crea tu página móvil";
$lang['title'] = "Título";
$lang['content'] = "Contenido";
$lang['link'] = "Enlazar";
$lang['embed'] = "Incrustar";
$lang['write below'] = "Escribe abajo";
$lang['embed from'] = "Incrusta contenido de YouTube, Flickr, SocialCam y";
$lang['more'] = "más";
$lang['email private'] = "Email (privado)";
$lang['publish'] = "Publicar";
$lang['photos'] = "Fotos";
$lang['how link'] = "[link: El texto del enlace | http://la_pagina_a_enlazar  ]";
$lang['how embed'] = "\n[embed: http://la_pagina_a_incrustar  ]";
$lang['how button'] = "\n[button: Texto | http://la_pagina_a_enlazar ]";
$lang['how menu'] = "\n[menu: Texto item 1 | http://la_pagina_a_enlazar ]\n[menu: Texto item 2 | http://la_pagina_a_enlazar ]\n[menu: Texto item 3 | http://la_pagina_a_enlazar ]";

//home
$lang['promote your'] ='Promueve tu negocio con';
$lang['in minutes'] = "Crea páginas móviles y códigos QR en minutos";
$lang['home CTA'] = "Crea tu página móvil (gratis)";
$lang['list 1'] = "Crea tu sitio móvil en minutos";
$lang['list 2'] = "Códigos QR listos para imprimir";
$lang['list 3'] = "Herramientas sociales incluidas";
$lang['list 4'] = "Tan fácil como hacer un sandwich";
$lang['howto ribbon'] = "Códigos QR: cómo usarlos ";
$lang['home ribbon'] = "Los códigos QR son enlaces web en el mundo real. ¡Usalos!";
$lang['plans and pricing'] = "Planes & precios";
$lang['photo credits'] = "Créditos fotográficos";
$lang['standard theme'] = "Tema standard";
$lang['random urls'] = "URL aleatoria";
$lang['ad supported'] = "Con publicidad";
$lang['10 pages'] = "10 páginas";
$lang['multiple themes'] = "Múltiples temas";
$lang['custom urls'] = "URL personalizada";
$lang['ads free'] = "Sin publicidad";
$lang['100 pages'] = "100 páginas";
$lang['Free'] = "Gratis";
$lang['Pro'] = "Pro";
$lang['demos and uses'] = "Demos y usos típicos";
$lang['restaurant'] = "Restaurant";
$lang['boutique'] = "Boutique";
$lang['professional'] = "Profesional";
$lang['real state'] = "Inmobiliario";
$lang['restaurant link'] = "codigos_qr_para_restaurantes";
$lang['boutique link'] = "codigos_qr_para_boutiques";
$lang['professional link'] = "codigos_qr_para_profesionales";
$lang['real state link'] = "codigos_qr_para_inmobiliarias";
$lang['slideshow_realstate'] = "Los agentes inmobiliatios usan los códigos QR para aumentar sus ventas.<br/><br/>
	Incluye fotos y videotours<br/><br/>Promoción cruzada entre tus propiedades.<br/><br/>
	No gastes tu tiempo en llamadas que una página web responde.";
$lang['slideshow_restaurant'] = "Usa fotos de tus platillos para que causar el antojo.<br/><br/>
	Promueve el menú del día en la ventana, haciéndolo accesible desde la calle.<br/><br/>
	Comparte algunas recetas, a tus clientes les encantará.";
$lang['slideshow_product'] = "Persuade un comprador indeciso en pleno supermercado con un vídeo.<br/><br/>
	Da a tus clientes toda la información necesaria:<br/><br/>
	\"Cuidados para tu planta.\"<br/>
	\"Escanea para nuestras recetas.\"<br/>	
	\"Compáranos con otras marcas.\"
	\"Descubre como fabricamos nuestra cerveza.\"<br/>";

/* editor */
$lang['header'] = "Encabezado";
$lang['text'] = "Texto";
$lang['button'] = "Botón";
$lang['menu'] = "Menú";
$lang['image'] = "Imagen";
$lang['editor instruction'] = "Arrastra y suelta estos elementos en tu página.";
$lang['move'] = "Mover";
$lang['select header size'] = "Selecciona una talla:";
$lang['add item'] = "Añadir item";
$lang['url'] = "Url";
$lang['upload an image'] = "Subir una imagen (jpeg or png)";
$lang['or pick an image'] = "O selecciona una de tu librería:";
$lang['click here to edit'] = "Haz clic aquí para escribir";
$lang['save & publish'] = "Guardar y publicar";
$lang['select theme'] = "Selecciona un tema:";
$lang['placeholder pagename'] ="Título de la página";
$lang['link to your pages'] = "O enlaza una de tus páginas";
$lang['enter a url'] = "Llena con un url";
$lang['examples'] = "Ejemplos";

//stats
$lang['stats'] = "Estadísticas";
$lang['pageviews'] = "Páginas vistas";
$lang['last 30 days'] = "Ultimos 30 días";

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */
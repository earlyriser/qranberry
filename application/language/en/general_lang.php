<?php
 
//FLASH
$lang['msg_password_changed'] 	= "Your password has been changed";
$lang['msg_page_added']		= "<b>This is your page.</b><br>1. <a href='*URL_ADD*'>Edit</a> your page if needed.<br>2. Print & paste your <a href='*URL_QR*'>QR code</a><br>3. That's all!";
$lang['msg_password_sent'] 	= "Check your email inbox (and spam folder). You will find a link to reset your password.";
$lang['msg_login_url']		= "Welcome back! Please change your password.";
$lang['msg_theme_updated'] = "Great! Your theme has been updated.";
$lang['msg_plan_updated'] = "Your plan has been updated.";

//Common
$lang['current_lang'] = "en";
$lang['account'] = "Account";
$lang['login'] = "Login";
$lang['logout'] = "Logout";
$lang['password'] = "Password";
$lang['email'] = "Email";
$lang['submit'] = "Submit";
$lang['tagline'] = "Create your mobile site & QR codes";
$lang['cancel'] = "Cancel";
$lang['delete'] = "Delete";
$lang['signup'] = "Signup";

//account
$lang['create page'] = "Create page";
$lang['change password'] = "Change password";
$lang['change plan'] = "Change plan";
$lang['pages'] = "Pages";
$lang['edit'] = "Edit";
$lang['profile'] = "Profile";
$lang['username'] = "Username";
$lang['homepage'] = "Homepage";
$lang['change homepage'] = "Change homepage";
$lang['the homepage is what people will see at'] ="The homepage is what people will see at";
$lang['do you really want to delete this page?'] ="Do you really want to delete this page?";

//account profile
$lang['change your password'] = "Change your password";
$lang['forgot it'] = "Forgot it?";
$lang['choose wisely'] = "Choose it wisely, because it will be permanent. At least 4 characters.";
$lang['your site will be at'] = "Your site will be:";
$lang["my_username"] = "my_username";

//account printing
$lang["printing"] = "Printing";
$lang["print step 1"] = "Download your mobile link";		
$lang["print step 2"] = "Print it";	
$lang["print step 3"] = "Use it in your business or product";	
$lang["ml resto"] = "Mobile links for restaurants";	
$lang["ml sale"] = "Mobile links for sale";	
$lang["ml rent"] = "Mobile links for rent";	
$lang["ml perso"] = "Personalized mobile link";	
$lang["qr standalone"] = "QR code standalone";	
$lang["download and integrate"] = "Download and integrate it in your design with your favorite image editor.";	
		
//password reminder
$lang['get your password'] = "Get your password";

//page
$lang['qr code'] = "QR Code";
$lang['print qr code'] = "Print QR code";

//footer
$lang['footer phrase'] = "Mobile page powered by";
$lang['TOS'] = "Terms of Service";
$lang['PP'] = "Privacy Policy";
$lang['contact'] = "Contact";

//page code
$lang['your title'] = "Your title";

//plans
$lang['plans'] = "Plans";
$lang['your current plan'] = "Your current plan:";
$lang['free'] = "FREE";
$lang['monthly'] = "monthly";
$lang['became Pro'] = "Become Pro";
$lang['interrupt plan'] = "Cancel Plan";
$lang['signup_plan'] = "Signup";

//style
$lang['style'] = "Style";
$lang['edit your css'] = "Edit your CSS";
$lang['save changes'] = "Save changes";
$lang['themes'] = 'Themes';

//add
$lang['create your mobile page'] = "Create your mobile page";
$lang['title'] = "Title";
$lang['content'] = "Content";
$lang['link'] = "Link";
$lang['embed'] = "Embed";
$lang['write below'] = "Write below";
$lang['embed from'] = "Embed from YouTube, Flickr, SocialCam and";
$lang['more'] = "more";
$lang['email private'] = "Email (private)";
$lang['publish'] = "Publish";
$lang['photos'] = "Photos";
$lang['how link'] = "[link: Link text | http://the_page_to_link ]";
$lang['how embed'] = "\n[embed: http://the_page_to embed ]";
$lang['how button'] = "\n[button: Button text | http://the_page_to_link ]";
$lang['how menu'] = "\n[menu: Item 1 text | http://the_page_to_link ]\n[menu: Item 2 text | http://the_page_to_link ]\n[menu: Item 3 text | http://the_page_to_link ]";

//home
$lang['promote your'] ='Promote your business with';
$lang['in minutes'] = "Create mobile pages & QR codes in minutes";
$lang['home CTA'] = "Create your site (free)";
$lang['list 1'] = "Create your mobile site in minutes";
$lang['list 2'] = "QR codes ready to print";
$lang['list 3'] = "Social tools included";
$lang['list 4'] = "Easy as making a sandwich";
$lang['howto ribbon'] = "QR codes: How to use them";
$lang['home ribbon'] = "QR codes are web links in the real world. Unleash their power!";
$lang['plans and pricing'] = "Plans & pricing";
$lang['photo credits'] = "Photo credits";
$lang['standard theme'] = "Standard theme";
$lang['random urls'] = "Random url";
$lang['ad supported'] = "Ad supported";
$lang['10 pages'] = "10 pages";
$lang['multiple themes'] = "Multiple themes";
$lang['custom urls'] = "Custom url";
$lang['ads free'] = "Ads free";
$lang['100 pages'] = "100 pages";
$lang['Free'] = "Free";
$lang['Pro'] = "Pro";
$lang['demos and uses'] = "Demos & use cases";
$lang['restaurant'] = "Restaurant";
$lang['boutique'] = "Boutique";
$lang['professional'] = "Professional";
$lang['real state'] = "Real estate";
$lang['restaurant link'] = "qr_codes_for_restaurants";
$lang['boutique link'] = "qr_codes_for_boutiques";
$lang['professional link'] = "qr_codes_for_professionals";
$lang['real state link'] = "qr_codes_for_real_estate";
$lang['slideshow_realstate'] = "Real estate agents use QR codes to get more leads<br/><br/>
	Include photos & video tours<br/><br/>Cross promote your properties<br/><br/>
	Save time by avoiding calls that a simple page of information can solve";
$lang['slideshow_restaurant'] = "Entice your customers with mouthwatering pictures<br/><br/>
	Promote the menu of the day on your window, making it accessible to folks working nearby<br/><br/>
	Share some of your recipes, customers will love it!";
$lang['slideshow_product'] = "Persuade an indecisive customer in the middle of a supermarket aisle with a video.<br/><br/>
	Give your customers all the information they need:<br/><br/>
	\"See how we craft this beer\"<br/>
	\"Scan to watch our best recipes\"<br/>
	\"How to take care of your plant\"<br/>
	\"Compare us with other brands\"";

/* editor */
$lang['header'] = "Header";
$lang['text'] = "Text";
$lang['button'] = "Button";
$lang['menu'] = "Menu";
$lang['image'] = "Image";
$lang['editor instruction'] = "Drag & drop the elements from the left to the page below.";
$lang['move'] = "Move";
$lang['select header size'] = "Select header size:";
$lang['add item'] = "Add item";
$lang['url'] = "Url";
$lang['upload an image'] = "Upload an image (jpg or png)";
$lang['or pick an image'] = "Or pick an image from your library:";
$lang['click here to edit'] = "Click here to write something";
$lang['save & publish'] = "Save & publish";
$lang['select theme'] = "Select a theme:";
$lang['placeholder pagename'] ="Page name";
$lang['link to your pages'] = "Or link one of your pages";
$lang['enter a url'] = "Enter a url";
$lang['examples'] = "Examples";

//stats
$lang['stats'] = "Stats";
$lang['pageviews'] = "Pageviews";
$lang['last 30 days'] = "Last 30 days";

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */
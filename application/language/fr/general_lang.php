<?php
 
//FLASH
$lang['msg_password_changed'] 	= "Votre mot de passe a été changé";
$lang['msg_page_added']		= "<b>Voici votre page</b><br>1. <a href='*URL_ADD*'>Éditer</a> si nécessaire. <br>2. Imprimez et collez votre <a href='*URL_QR*'>code QR</a><br>3. Voilà! C'est fait.";
$lang['msg_password_sent'] 	= "Vérifiez votre courriel (et le dossier spam). Vous y trouverez un lien pour modifier votre mot de passe.";
$lang['msg_login_url']		= "Bonjour. Changez votre mot de passe.";
$lang['msg_theme_updated'] = "Excellent! Votre thème a été mis à jour.";
$lang['msg_plan_updated'] = "Votre plan a été mis à jour.";

//Common
$lang['current_lang'] = "fr";
$lang['account'] = "Mon compte";
$lang['login'] = "Connexion";
$lang['logout'] = "Déconnexion";
$lang['password'] = "Mot de passe";
$lang['email'] = "Courriel";
$lang['submit'] = "Envoyer";
$lang['tagline'] = "Pages avec codes QR gratuits.";
$lang['cancel'] = "Annuler";
$lang['delete'] = "Supprimer";
$lang['signup'] = "Inscription";

//account
$lang['create page'] = "Créer ma page";
$lang['change password'] = "Changer mon mot de passe";
$lang['change plan'] = "Changer mon plan";
$lang['pages'] = "Pages";
$lang['edit'] = "Editer";
$lang['profile'] = "Profil";
$lang['username'] = "Nom d'utilisateur";
$lang['homepage'] = "Page d'accueil";
$lang['change homepage'] = "Changer la page d'accueil";
$lang['the homepage is what people will see at'] ="La page d'accueil est celle-ci";
$lang['do you really want to delete this page?'] ="Voulez-vous vraiment supprimer cette page?";

//account profile
$lang['change your password'] = "Changer votre mot de passe";
$lang['forgot it'] = "Mot de passe oublié?";
$lang['choose wisely'] = "Choisissez bien car ce sera permanent. Au moins 4 caractères.";
$lang['your site will be at'] = "Votre site sera:";
$lang["my_username"] = "mon_nom_d_utilisateur";

//account printing
$lang["printing"] = "Impression";
$lang["print step 1"] = "Téléchargez votre lien mobile";
$lang["print step 2"] = "Imprimez-le";
$lang["print step 3"] = "Utilisez-le dans votre entreprise ou produit";
$lang["ml resto"] = "Liens mobiles pour restaurants";
$lang["ml sale"] = "Liens mobiles pour vendre";
$lang["ml rent"] = "Liens mobiles pour louer";
$lang["ml perso"] = "Lien mobile personnalisé";
$lang["qr standalone"] = "Code QR seulement";
$lang["download and integrate"] = "Téléchargez le code QR et intégrez-le dans votre propre design en utilisant votre logiciel favori.";

//password reminder
$lang['get your password'] = "Récupérez votre mot de passe";

//page
$lang['qr code'] = "Code QR";
$lang['print qr code'] = "Imprimer code QR";

//footer
$lang['footer phrase'] = "Page mobile faite avec ";
$lang['TOS'] = "Termes et conditions";
$lang['PP'] = "Politique de confidentialité";
$lang['contact'] = "Contact";

//page code
$lang['your title'] = "Votre titre";

//plans
$lang['plans'] = "Plans";
$lang['your current plan'] = "Votre plan actuel:";
$lang['free'] = "GRATUIT";
$lang['monthly'] = "mensuel";
$lang['became Pro'] = "Passer à Pro";
$lang['interrupt plan'] = "Annuler plan";
$lang['signup_plan'] = "Souscrire";

//style
$lang['style'] = "Style";
$lang['edit your css'] = "Editez votre CSS";
$lang['save changes'] = "Enregistrer changements";
$lang['themes'] = "Thèmes";

//add
$lang['create your mobile page'] = "Créez votre page mobile";
$lang['title'] = "Titre";
$lang['content'] = "Contenu";
$lang['link'] = "Ajouter lien";
$lang['embed'] = "Joindre fichier";
$lang['write below'] = "Écrire ci-bas";
$lang['embed from'] = "Intégrer contenu provenant de YouTube, Flickr, SocialCam et";
$lang['more'] = "plus";
$lang['email private'] = "Courriel (privé)";
$lang['publish'] = "Publier";
$lang['photos'] = "Photos";
$lang['how link'] = '[link: http://la_page_du_lien | Le texte du lien]';
$lang['how embed'] = '[embed: http://la_page_a_joindre ]';
$lang['how button'] = "\n[button: Button text | http://la_page_a_lier ]";
$lang['how menu'] = "\n[menu: Item 1 texte | http://la_page_a_lier ]\n[menu: Item 2 texte | http://la_page_a_lier ]\n[menu: Item 3 texte | http://la_page_a_lier ]";

//home
$lang['promote your'] ='Votre entreprise présente partout avec';
$lang['in minutes'] = "Créez votre site mobile & codes QR en quelques minutes";
$lang['home CTA'] = "Créez votre site (gratuit)";
$lang['list 1'] = "Créez votre site mobile en quelques minutes";
$lang['list 2'] = "Codes QR prêts à imprimer";
$lang['list 3'] = "Outils pour médias sociaux inclus";
$lang['list 4'] = "Aussi facile que de faire un sandwich";
$lang['howto ribbon'] = "Codes QR : Mode d'emploi";
$lang['home ribbon'] = "Découvrez les codes QR et tout leur pouvoir avec cette vidéo";
$lang['plans and pricing'] = "Plans et tarifs";
$lang['photo credits'] = "Crédits photos";
$lang['standard theme'] = "Thème standard";
$lang['random urls'] = "Url aléatoire";
$lang['ad supported'] = "Présence de publicité";
$lang['10 pages'] = "10 pages";
$lang['multiple themes'] = "Thèmes multiples";
$lang['custom urls'] = "Url personnalisés";
$lang['ads free'] = "Sans publicité";
$lang['100 pages'] = "100 pages";
$lang['Free'] = "Gratuit";
$lang['Pro'] = "Pro";
$lang['demos and uses'] = "Démos & exemples d'utilisation";
$lang['restaurant'] = "Restaurant";
$lang['boutique'] = "Boutique";
$lang['professional'] = "Professionnel";
$lang['real state'] = "Immobilier";
$lang['restaurant link'] = "codes_qr_pour_restaurants";
$lang['boutique link'] = "codes_qr_pour_boutiques";
$lang['professional link'] = "codes_qr_pour_les_profesionnels";
$lang['real state link'] = "codes_qr_pour_l_inmobillier";
$lang['slideshow_realstate'] = "Les agents immobiliers utilisent les codes QR pour augmenter leurs ventes<br/><br/>
	Ajout de photos et de visites virtuelles<br/><br/>
	Promotion croisée entre propriétés<br/><br/>
	Économie de temps en donnant toutes les infos par le web";
$lang['slideshow_restaurant'] = "Mettez l'eau à la bouche de vos clients avec des photos de vos plats<br/><br/>
	Affichez le menu du jour dans votre vitrine et rendez-le accessible depuis la rue<br/><br/>
	Partagez certaines de vos recettes, vos clients adoreront ça!";
$lang['slideshow_product'] = "Persuadez les clients indécis directement au supermarché:<br/><br/>
	\"Voyez comment nous brassons cette bière\"<br/>
	\"Scannez et voyez nos meilleures recettes\"<br/>
	\"Comment prendre soin de cette plante\"<br/>
	\"Comparez-nous avec les autres marques populaires\"";
	
//editor
$lang['header'] = "En-tête";
$lang['text'] = "Texte";
$lang['button'] = "Bouton";
$lang['menu'] = "Menu";
$lang['image'] = "Image";
$lang['editor instruction'] = "Glisser-déposer ces éléments vers votre page";
$lang['move'] = "Déplacer";
$lang['select header size'] = "Sélectionner la grandeur";
$lang['add item'] = "Ajouter item";
$lang['url'] = "Url";
$lang['upload an image'] = "Ajouter une image";
$lang['or pick an image'] = "Ou choisir dans votre librairie";
$lang['click here to edit'] = "Cliquer ici pour écrire";
$lang['save & publish'] = "Sauvegarder & publier";
$lang['select theme'] = "Sélectionner un thème";
$lang['placeholder pagename'] = "Titre de la page";
$lang['link to your pages'] = "Ou mettre un lien vers une vos pages";
$lang['enter a url'] = "Saisissez une url";
$lang['examples'] = "Exemples";

//stats
$lang['stats'] = "Statistique";
$lang['pageviews'] = "Pages vues";
$lang['last 30 days'] = "30 derniers jours";
/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */
<?php

//PASSWORD 
$lang['email_password_subject'] = "Qranberry Password reset"; 

$lang['email_password_content']		= <<<EOM
You're receiving this email because you requested a password reset for your user account at Qranberry.

Please go to the following page and choose a new password:
**LINK**

Best regards,
The Qranberry team
EOM;
//-------------------------------


//CONFIRMATION
$lang['email_confirmation_subject'] = 'Qranberry Confirmation email';

$lang['email_confirmation_content']	 = <<<EOM
Welcome to Qranberry!
	
Visit this link to confirm that you own this email address:
**LINK**
	
If you've received this email by mistake, simply delete it.
	
Best regards,
The Qranberry team
EOM;
//------------------------------



//PRO SUBSCRIBE
$lang['email_pro_subscribe_subject'] = 'Votre compte Qranberry Pro';

$lang['email_pro_subscribe_content'] = <<<EOM

Bonjour!

Votre nouveau compte Pro est pr�t.

Il y a trois choses que vous pourriez vouloir faire � partir du panneau de contr�le de votre compte:
1. S�lectionner un nom d'utilisateur pour rendre votre site accessible � partir de qranberry.me/nom_d_utilisateur
2. S�lectionner la page qui sera votre page d'accueil parmi toutes les pages de votre site
3. Choisir un th�me

N'h�sitez pas � nous �crire si vous �prouvez des difficult�s.

Cordialement,
L'�quipe de Qranberry
EOM;

//--------------------------------




//WELCOME
$lang['email_pro_subscribe_subject'] = 'Bienvenue sur Qranberry';

$lang['email_pro_subscribe_content'] = <<<EOM

Bonjour!

J'ai vu que vous vous aviez ouvert un compte sur Qranberry. Je vous souhaite la bienvenue et j'esp�re notre application vous sera utile.

Si vous avec des questions, suggestions, bogues ou si vous �tes tout simplement heureux, je serai ravi d'entendre tout �a!

Au plaisir,
Roberto Martinez
Fondateur
EOM;

//--------------------------------


$lang['email_plan_unsubscribe'] ="";

/* End of file about_lang.php */
/* Location: ./system/language/fr/about_lang.php */
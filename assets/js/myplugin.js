(function( $ ){

  $.fn.maxHeight = function() {
    var max = 0;
    this.each(function() {
      max = Math.max( max, $(this).height() );
    });
    return max;
  };


	//SHOWTOGGLE
	$.fn.showtoggle = function() 
	{
		$(".content_hidden").css("display","none");	//hide on load

		$(".content_switch").click(function(e)		//on click
		{	
			show_hide ( this );
			e.preventDefault()
		});
	};


	//SHOWTOGGLE_RADIO 
	//.radio_switch modifies id named as (this).attr('data-interact-id')
	$.fn.showtoggle_radio = function() 
	{
		//when radio button is modified
		$(".radio_switch").change(function() 
		{
			//if it has a data-interact-id, show
			if ( $(this).attr( 'data-interact-id') )
			{
				$("#" + $ (this).attr('data-interact-id') ).show();
			}
			//if not, hide all the ids dependent of this radio set
			else
			{				
				var radio_name = $(this).attr( 'name' );
				//for each radio button that have this name
				$("input:radio[name="+radio_name+"]").each(function() 
				{
					if ( $ (this).attr('data-interact-id') )	//if it has a data-interact-id
					{
						$("#" + $ (this).attr('data-interact-id') ).hide(); //hide it
					}
				});
			}
		});	
	};


	//QR_INFO
	$.fn.qr_info = function() 
	{
		$('#qr_show').click(function(e)
		{
			if ( $(this).attr('data-loaded') == "false" )
			{
				var qrurl = $(this).attr('data-qrurl'); 
				var img_str = '<img class="image" src="'+qrurl+'">';
				$('#qr_info').append( img_str );
				
				$(this).attr('data-loaded', "true");
				
				e.preventDefault();
			}
		});
	};

	//HIDE IMAGES
	//hide images in serie when they were previously pasted
	$.fn.hide_images = function() 
	{
		$(".pasted_image").each(function()	//for each pasted img
		{
			var serial_image = $(this).attr( 'src' );	//get its src
			$('img[src="'+serial_image+'"]').last().parent().remove(); //hide last img with this src
		});
	};

	//show/hide id based on interactor behavior
	function show_hide ( content_switch )
	{
			var content_hidden_id = $(content_switch).attr('data-interact-id');	//get id of changing content			
			//show
			if ( $('#'+content_hidden_id).attr('data-toggle') == '0' || $('#'+content_hidden_id) == undefined )
			{	
				//change display
				$("#"+content_hidden_id).show(); 
				//change state
				$('#'+content_hidden_id).attr('data-toggle', '1');
			}
			//hide
			else
			{	
				//change display
				$("#"+content_hidden_id).hide();
				//change state
				$('#'+content_hidden_id).attr('data-toggle', '0');
			}
			return false;
	}



	//INSERT AT CARET
	$.fn.insertAtCaret = function (tagName) {
		return this.each(function(){
			if (document.selection) {
				//IE support
				this.focus();
				sel = document.selection.createRange();
				sel.text = tagName;
				this.focus();
			}else if (this.selectionStart || this.selectionStart == '0') {
				//MOZILLA/NETSCAPE support
				startPos = this.selectionStart;
				endPos = this.selectionEnd;
				scrollTop = this.scrollTop;
				this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
				this.focus();
				this.selectionStart = startPos + tagName.length;
				this.selectionEnd = startPos + tagName.length;
				this.scrollTop = scrollTop;
			} else {
				this.value += tagName;
				this.focus();
			}
		});
	};

	//DELETE_PHOTO
	$.fn.delete_photo = function() 
	{
		$(".delete_photo").click(function (e)
		{
			// php delete
			alert('delete photo');
			e.preventDefault();
		});
	};
	
})( jQuery );